# -*- coding: utf-8 -*-

import sys

reload(sys)
import locale

sys.setdefaultencoding(locale.getpreferredencoding())

import os
import logging
import logging.handlers
import argparse
# import sip
# sip.setapi('QVariant',2)  # crashes editing
from camelot.core.conf import settings, SimpleSettings
from population.view import setup_views
from population.constant import update_fixtures
from population.application_admin import LOGNAME
# from kladr.utils import log_exceptions

from camelot.view.forms import Form
from camelot.admin.entity_admin import EntityAdmin

logging.basicConfig(level=logging.ERROR)
logger = logging.getLogger('main')


class Auth(EntityAdmin):
    form_display = Form(['name', 'password'])


# begin custom settings
class MySettings(SimpleSettings):
    def setup_model(self):
        """This function will be called at application.py startup, it is used to
        setup the model"""
        from camelot.core.sql import metadata
        from camelot.core.orm import setup_all
        # metadata.bind = self.ENGINE()
        import camelot.model.authentication
        import camelot.model.i18n
        import camelot.model.fixture  # it cant't work without this
        import camelot.model.memento
        import population.model
        setup_all()
        metadata.create_all()

        update_fixtures()
        setup_views()

    def ENGINE(self):
        import urllib
        import pyodbc
        from sqlalchemy import create_engine

        # def get_driver():
        #     usable = [d for d in pyodbc.drivers() if 'SQL Server' in d]
        #     if not usable:
        #         raise Exception('No usable ODBC driver')
        #
        #     def weight(val):
        #         w = 0
        #         if 'Native Client' in val:
        #             w += 2
        #         if 'ODBC Driver' in val:
        #             w += 1
        #         return w
        #
        #     return max(usable, key=weight)


        # return create_engine('mssql+pyodbc://localhost/kladr?driver=%s' % urllib.quote_plus(get_driver()),
        #                      isolation_level='SERIALIZABLE')
        # return create_engine('mssql+pyodbc://kladr_dsn', isolation_level='SERIALIZABLE')


my_settings = MySettings('CRB', 'population')
settings.append(my_settings)


# end custom settings

def start_application():
    # from camelot.view.main import main
    from camelot.view.main import main_action
    from population.application_admin import MyApplicationAdmin
    #from population.application import Application
    import population.application as application
    parser = argparse.ArgumentParser()
    parser.add_argument("-A", "--admin", action="store_true",
                        help="switches on administrator features")
    app_admin = MyApplicationAdmin()
    app_admin.app_args = parser.parse_args()
    application.the_application = application.MyApplication(app_admin)
    main_action(application.the_application)
    # main(MyApplicationAdmin())


if __name__ == '__main__':
    logging.basicConfig(level=logging.ERROR)
    logger = logging.getLogger('main')
    logger.setLevel(logging.INFO)
    handler = logging.handlers.RotatingFileHandler(
        os.path.expanduser('~/%s' % LOGNAME), maxBytes=1024 * 256, backupCount=2)
    handler.setFormatter(logging.Formatter(u'%(levelname)-8s | %(asctime)s | %(message)s'))
    logger.addHandler(handler)

    # date, time, ok = DateDialog.getDateTime()
    start_application()
