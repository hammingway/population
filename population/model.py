# -*- coding: utf-8 -*-

import sys
from collections import OrderedDict
from inspect import isclass

from camelot.admin import table
from camelot.admin.action.list_filter import ComboBoxFilter, EditorFilter
from camelot.admin.entity_admin import EntityAdmin
from camelot.core.orm import Entity, ManyToOne, OneToMany, ManyToMany, ColumnProperty, using_options
from camelot.core.utils import ugettext_lazy as _
from sqlalchemy import func, sql, event, DDL, BigInteger, Unicode, Date, Time, Boolean, Index
from sqlalchemy.schema import Column
#from citext import CIText

import requests
from specs import specs_all, iter_specs


# class Column(sqlalchemy_Column):
#     def __init__(self, *args, **kwargs):
#         if (len(args) > 0 and isinstance(args[0], Unicode)
#                 or len(args) > 1 and isinstance(args[0], (str, unicode)) and isinstance(args[1], Unicode)
#                 or isinstance(kwargs.get('type_'), Unicode)):
#             kwargs.update(server_default=u'')
#         super(Column, self).__init__(*args, **kwargs)


class Kladr(Entity):
    # temporary
    #__table_args__ = {'prefixes': ['TEMPORARY']}
    __tablename__ = 'kl_kladr'
    name = Column(Unicode(40))
    socr = Column(Unicode(10))
    code = Column(Unicode(13))
    index = Column(Unicode(6))
    gninmb = Column(Unicode(4))
    uno = Column(Unicode(4))
    ocatd = Column(Unicode(11))
    status = Column(Unicode(1))

    class Admin(EntityAdmin):
        verbose_name_plural = _('Kladr')
        list_display = ['name', 'socr', 'code', 'index', 'gninmb', 'uno', 'ocatd', 'status']


Index('ix_%s_code' % Kladr.__tablename__, Kladr.code)


class KladrStreet(Entity):
    # temporary
    #__table_args__ = {'prefixes': ['TEMPORARY']}
    __tablename__ = 'kl_street'
    name = Column(Unicode(40))
    socr = Column(Unicode(10))
    code = Column(Unicode(17))
    index = Column(Unicode(6))
    gninmb = Column(Unicode(4))
    uno = Column(Unicode(4))
    ocatd = Column(Unicode(11))

    class Admin(EntityAdmin):
        verbose_name_plural = _('Street')
        list_display = ['name', 'socr', 'code', 'index', 'gninmb', 'uno', 'ocatd']


Index('ix_%s_code' % KladrStreet.__tablename__, KladrStreet.code)


class KladrDoma(Entity):
    # temporary
    #__table_args__ = {'prefixes': ['TEMPORARY']}
    __tablename__ = 'kl_doma'
    name = Column(Unicode(40))
    numb = Column(BigInteger())
    korp = Column(Unicode(10))
    socr = Column(Unicode(10))
    code = Column(Unicode(19))
    index = Column(Unicode(6))
    gninmb = Column(Unicode(4))
    uno = Column(Unicode(4))
    ocatd = Column(Unicode(11))

    class Admin(EntityAdmin):
        verbose_name_plural = _('Doma')
        list_display = ['name', 'numb', 'korp', 'socr', 'code', 'index', 'gninmb', 'uno', 'ocatd']


Index('ix_%s_code' % KladrDoma.__tablename__, KladrDoma.code)


class KladrAltnames(Entity):
    # temporary
    #__table_args__ = {'prefixes': ['TEMPORARY']}
    __tablename__ = 'kl_altnames'
    oldcode = Column(Unicode(19))
    newcode = Column(Unicode(19))
    level = Column(Unicode(1))

    class Admin(EntityAdmin):
        verbose_name_plural = _('Altnames')
        list_display = ['oldcode', 'newcode', 'level']


Index('ix_%s_oldcode' % KladrAltnames.__tablename__, KladrAltnames.oldcode)
Index('ix_%s_newcode' % KladrAltnames.__tablename__, KladrAltnames.newcode)


################################################################################


class Town(Entity):
    # using_options(order_by=['socr', 'name'])
    __tablename__ = 'town'
    street = OneToMany('Street')
    house = OneToMany('House')
    name = Column(Unicode(40))
    socr = Column(Unicode(10))
    codes = Column(Unicode(2))
    coder = Column(Unicode(3))
    codeg = Column(Unicode(3))
    codep = Column(Unicode(3))
    index = Column(Unicode(6))
    gninmb = Column(Unicode(4))
    uno = Column(Unicode(4))
    ocatd = Column(Unicode(11))
    status = Column(Unicode(1))

    class Admin(EntityAdmin):
        verbose_name = _('Town')
        verbose_name_plural = _('Towns')
        field_attributes = {'street': {'name': u'Улицы'},
                            'name': {'name': u'Наименование'},
                            'socr': {'name': u'Тип'},
                            'codes': {'editable': False},
                            'coder': {'editable': False},
                            'codeg': {'editable': False},
                            'codep': {'editable': False}
                            }
        list_display = ['name', 'socr', 'codes', 'coder', 'codeg', 'codep', 'index', 'gninmb', 'uno',
                        'ocatd', 'status']
        form_display = ['name', 'socr', 'codes', 'coder', 'codeg', 'codep', 'index', 'gninmb', 'uno',
                        'ocatd', 'status', 'street', 'house']

    def __unicode__(self):
        full = []
        if self.socr:
            full.append(self.socr)
        if self.name:
            full.append(self.name)
        return u' '.join(full)


Index('ix_%s_name' % Town.__tablename__, func.lower(Town.name))
Index('ix_%s_codes' % Town.__tablename__, Town.codes)
Index('ix_%s_coder' % Town.__tablename__, Town.coder)
Index('ix_%s_codeg' % Town.__tablename__, Town.codeg)
Index('ix_%s_codep' % Town.__tablename__, Town.codep)
Index('ix_%s_index' % Town.__tablename__, Town.index)
Index('ix_%s_ocatd' % Town.__tablename__, Town.ocatd)


class Street(Entity):
    # using_options(order_by=['town_id', 'socr', 'name'])
    __tablename__ = 'street'
    town = ManyToOne('Town', required=True, backref='street', ondelete='cascade')
    house = OneToMany('House')
    name = Column(Unicode(40))
    socr = Column(Unicode(10))
    codes = Column(Unicode(2))
    coder = Column(Unicode(3))
    codeg = Column(Unicode(3))
    codep = Column(Unicode(3))
    codeu = Column(Unicode(4))
    index = Column(Unicode(6))
    gninmb = Column(Unicode(4))
    uno = Column(Unicode(4))
    ocatd = Column(Unicode(11))
    status = Column(Unicode(1))

    class Admin(EntityAdmin):
        verbose_name = _('Street')
        verbose_name_plural = _('Streets')
        field_attributes = {'town': {'name': u'Город'},
                            'house': {'name': u'Дома'},
                            'name': {'name': u'Наименование'},
                            'socr': {'name': u'Тип'},
                            'codes': {'editable': False},
                            'coder': {'editable': False},
                            'codeg': {'editable': False},
                            'codep': {'editable': False},
                            'codeu': {'editable': False}
                            }
        list_display = ['town', 'name', 'socr', 'codes', 'coder', 'codeg', 'codep', 'codeu', 'index',
                        'gninmb', 'uno', 'ocatd', 'status']
        form_display = ['town', 'name', 'socr', 'codes', 'coder', 'codeg', 'codep', 'codeu', 'index',
                        'gninmb', 'uno', 'ocatd', 'status', 'house']

    def __unicode__(self):
        full = []
        if self.town:
            full.append(unicode(self.town))
        if self.socr:
            full.append(self.socr)
        if self.name:
            full.append(self.name)
        return u' '.join(full)


Index('ix_%s_name' % Street.__tablename__, func.lower(Street.name))
Index('ix_%s_socr' % Street.__tablename__, func.lower(Street.socr))
Index('ix_%s_codes' % Street.__tablename__, Street.codes)
Index('ix_%s_coder' % Street.__tablename__, Street.coder)
Index('ix_%s_codeg' % Street.__tablename__, Street.codeg)
Index('ix_%s_codep' % Street.__tablename__, Street.codep)
Index('ix_%s_codeu' % Street.__tablename__, Street.codeu)
Index('ix_%s_index' % Street.__tablename__, Street.index)
Index('ix_%s_ocatd' % Street.__tablename__, Street.ocatd)


class House(Entity):
    # using_options(order_by=['street_id', 'socr', 'name'])
    __tablename__ = 'house'
    town = ManyToOne('Town', required=True, backref='house')
    street = ManyToOne('Street', required=False, backref='house', ondelete='cascade')
    division = ManyToMany('Division',
                          tablename='house_division__division_house',
                          local_colname='house_id',
                          remote_colname='division_id',
                          order_by=['spec', 'name', 'vr'],
                          ondelete='cascade')
    patient = OneToMany('Patient')
    name = Column(Unicode(40))
    numb = Column(BigInteger)
    socr = Column(Unicode(10))
    codes = Column(Unicode(2))
    coder = Column(Unicode(3))
    codeg = Column(Unicode(3))
    codep = Column(Unicode(3))
    codeu = Column(Unicode(4))
    coded = Column(Unicode(4))
    index = Column(Unicode(6))
    gninmb = Column(Unicode(4))
    uno = Column(Unicode(4))
    ocatd = Column(Unicode(11))
    status = Column(Unicode(1))

    class Admin(EntityAdmin):
        verbose_name = _('House')
        verbose_name_plural = _('Houses')
        list_filter = [ComboBoxFilter('town.name'), ComboBoxFilter('street.name'), EditorFilter('numb'),
                       ComboBoxFilter('division.name')]
        field_attributes = {'street': {'name': u'Улица'},
                            'division': {'name': u'Участки'},
                            'patient': {'name': u'Пациенты'},
                            'name': {'name': u'Номер'},
                            'numb': {'name': u'Номер числом'},
                            'codes': {'editable': False},
                            'coder': {'editable': False},
                            'codeg': {'editable': False},
                            'codep': {'editable': False},
                            'codeu': {'editable': False},
                            'coded': {'editable': False}
                            }
        list_display = table.Table([
            table.ColumnGroup(_('General'),
                              ['town', 'street', 'socr', 'name', 'index']),
            table.ColumnGroup(_('All'),
                              ['town', 'street', 'socr', 'name', 'numb', 'codes', 'coder', 'codeg', 'codep', 'codeu',
                               'coded', 'index', 'gninmb', 'uno', 'ocatd', 'status'])])
        list_search = ['street.name', 'street.town.name', 'name']
        form_display = ['town', 'street', 'name', 'numb', 'socr', 'codes', 'coder', 'codeg', 'codep', 'codeu', 'coded',
                        'index', 'gninmb', 'uno', 'ocatd', 'status', 'division', 'patient']

    def __unicode__(self):
        full = []
        if self.street:
            full.append(unicode(self.street))
        elif self.town:
            full.append(unicode(self.town))
        if self.socr:
            full.append(self.socr)
        if self.name:
            full.append(self.name)
        return u' '.join(full)


Index('ix_%s_name' % House.__tablename__, func.lower(House.name))
Index('ix_%s_numb' % House.__tablename__, House.numb)
Index('ix_%s_codes' % House.__tablename__, House.codes)
Index('ix_%s_coder' % House.__tablename__, House.coder)
Index('ix_%s_codeg' % House.__tablename__, House.codeg)
Index('ix_%s_codep' % House.__tablename__, House.codep)
Index('ix_%s_codeu' % House.__tablename__, House.codeu)
Index('ix_%s_coded' % House.__tablename__, House.coded)
Index('ix_%s_index' % House.__tablename__, House.index)
Index('ix_%s_ocatd' % House.__tablename__, House.ocatd)


################################################################################


class DivisionDBF(Entity):
    # temporary
    #__table_args__ = {'prefixes': ['TEMPORARY']}
    __tablename__ = 'dbf_division'
    n = Column(BigInteger())
    name = Column(Unicode(40))
    okrug = Column(Unicode(23))
    punkt_1 = Column(Unicode(44))
    ulica_1 = Column(Unicode(54))
    gs = Column(Unicode(9))
    vse_n = Column(BigInteger())
    vse_e = Column(BigInteger())
    nech_n = Column(BigInteger())
    nech_e = Column(BigInteger())
    chet_n = Column(BigInteger())
    chet_e = Column(BigInteger())
    spis = Column(Unicode(9))
    uch_vz = Column(Unicode(9))
    vz_vr = Column(Unicode(18))
    sni_vzr = Column(Unicode(19))
    uch_det = Column(Unicode(16))
    det_vr = Column(Unicode(32))
    sni_det = Column(Unicode(36))
    uch_ven = Column(Unicode(9))
    cla_prom = Column(Unicode(40))
    cladr = Column(Unicode(40))
    street = Column(Unicode(40))
    cla_end = Column(Unicode(40))
    ss_end = Column(Unicode(40))
    cla_soc = Column(Unicode(10))
    cla_cod = Column(Unicode(13))
    str_end = Column(Unicode(40))
    str_soc = Column(Unicode(10))
    str_cod = Column(Unicode(17))

    class Admin(EntityAdmin):
        verbose_name = _('Division (DBF)')
        verbose_name_plural = _('Divisions (DBF)')
        list_display = ['n', 'okrug', 'punkt_1', 'ulica_1', 'gs', 'vse_n', 'vse_e', 'nech_n', 'nech_e', 'chet_n',
                        'chet_e', 'spis', 'uch_vz', 'vz_vr', 'sni_vzr', 'uch_det', 'det_vr', 'sni_det', 'uch_ven',
                        'cla_prom', 'cladr', 'street', 'cla_end', 'ss_end', 'cla_soc', 'cla_cod', 'str_end', 'str_soc',
                        'str_cod']


Index('ix_%s_uch_vz' % DivisionDBF.__tablename__, func.lower(DivisionDBF.uch_vz))
Index('ix_%s_vz_vr' % DivisionDBF.__tablename__, func.lower(DivisionDBF.vz_vr))
Index('ix_%s_uch_det' % DivisionDBF.__tablename__, func.lower(DivisionDBF.uch_det))
Index('ix_%s_det_vr' % DivisionDBF.__tablename__, func.lower(DivisionDBF.det_vr))
Index('ix_%s_uch_ven' % DivisionDBF.__tablename__, func.lower(DivisionDBF.uch_ven))


class HouseDivision(Entity):
    # temporary
    #__table_args__ = {'prefixes': ['TEMPORARY']}
    __tablename__ = 'house_division'
    house_id = Column(BigInteger(), primary_key=True)  # ManyToOne('House', required=True)
    division_id = Column(BigInteger(), primary_key=True)  # ManyToOne('Division', required=True)


class HousesRange(Entity):
    # using_options(order_by=['division_id', 'cla_soc', 'name', 'str_soc', 'str_end'])

    __tablename__ = 'houses_range'
    division = ManyToOne('Division', required=False, ondelete='set null')
    name = Column(Unicode(40))
    cla_end = Column(Unicode(40))
    cla_soc = Column(Unicode(10))
    str_end = Column(Unicode(40))
    str_soc = Column(Unicode(10))
    gs = Column(Unicode(9))
    nech_n = Column(BigInteger())
    nech_e = Column(BigInteger())
    chet_n = Column(BigInteger())
    chet_e = Column(BigInteger())

    class Admin(EntityAdmin):
        verbose_name = _('Houses range')
        verbose_name_plural = _('Houses ranges')
        field_attributes = {'division': {'name': u'Участок'},
                            'name': {'name': u'Н.Пункт по КЛАДР'},
                            'cla_end': {'name': u'Н.Пункт'},
                            'cla_soc': {'name': u'Тип н.п.'},
                            'str_end': {'name': u'Улица'},
                            'str_soc': {'name': u'Тип улицы'},
                            'nech_n': {'name': u'Нечетные с'},
                            'nech_e': {'name': u'Нечетные по'},
                            'chet_n': {'name': u'Четные с'},
                            'chet_e': {'name': u'Четные по'}
                            }
        list_display = ['division', 'name', 'cla_end', 'cla_soc', 'str_end', 'str_soc', 'gs', 'nech_n', 'nech_e',
                        'chet_n', 'chet_e']
        form_display = ['division', 'name', 'cla_end', 'cla_soc', 'str_end', 'str_soc', 'gs', 'nech_n', 'nech_e',
                        'chet_n', 'chet_e']

    def __unicode__(self):
        return u'%s %s %s %s н(%s - %s) ч(%s - %s)' \
               % (self.cla_soc or '',
                  self.name or '',
                  self.str_soc or '',
                  self.str_end or '',
                  self.nech_n or '',
                  self.nech_e or '',
                  self.chet_n or '',
                  self.chet_e or '')


Index('ix_%s_name' % HousesRange.__tablename__, func.lower(HousesRange.name))
Index('ix_%s_cla_end' % HousesRange.__tablename__, func.lower(HousesRange.cla_end))
Index('ix_%s_cla_soc' % HousesRange.__tablename__, func.lower(HousesRange.cla_soc))
Index('ix_%s_str_end' % HousesRange.__tablename__, func.lower(HousesRange.str_end))
Index('ix_%s_str_soc' % HousesRange.__tablename__, func.lower(HousesRange.str_soc))

trigger = DDL('''
    CREATE OR REPLACE FUNCTION houses_range_trigg_null_func()
      RETURNS trigger AS
    $BODY$
    begin
      %s
    return new;
    end;
    $BODY$
      LANGUAGE plpgsql VOLATILE;

    CREATE TRIGGER trigg_null
    BEFORE INSERT OR UPDATE ON "houses_range"
    FOR EACH ROW
    EXECUTE PROCEDURE houses_range_trigg_null_func();
''' % ''.join('''if (NEW.%s = '') then
  NEW.%s = NULL;
end if;
''' % (i, i) for i in ('name', 'cla_end', 'cla_soc', 'str_end', 'str_soc', 'gs')))

event.listen(
    HousesRange.table,
    'after_create',
    trigger.execute_if(dialect='postgresql')
)


class Division(Entity):
    # using_options(order_by=['spec', 'name', 'vr'])
    __tablename__ = 'division'
    house = ManyToMany('House',
                       tablename='house_division__division_house',
                       local_colname='division_id',
                       remote_colname='house_id',
                       ondelete='cascade')
    patient = ManyToMany('Patient',
                         tablename='patient_division__division_patient',
                         local_colname='division_id',
                         remote_colname='patient_id',
                         ondelete='cascade')  # ,  order_by=['fam', 'im', 'ot', 'drd']
    hrange = OneToMany('HousesRange')
    name = Column(Unicode(20))
    vr = Column(Unicode(32))
    sni = Column(Unicode(36))
    spec = Column(Unicode(20))

    # @ColumnProperty
    # def childc(self):
    #     hdt = Division.metadata.tables['house_division__division_house']
    #     return sql.select([sql.func.sum(hdt.c.division_id)], hdt.c.division_id == self.id)

    class Admin(EntityAdmin):
        verbose_name = _('Division')
        verbose_name_plural = _('Divisions')
        field_attributes = {'house': {'name': u'Дома'},
                            'patient': {'name': u'Пациенты'},
                            'hrange': {'name': u'Диапазоны домов'},
                            'name': {'name': u'Код'},
                            'vr': {'name': u'Врач'},
                            'sni': {'name': u'Снилс'},
                            'spec': {'name': u'Специализация',
                                     'choices': lambda o: [
                                         (u'взрослые', _('Mature')),
                                         (u'педиатр', _('Child doctor')),
                                         (u'венеролог', _('Venereal')),
                                         (None, _('Unknown'))]}}
        list_display = ['name', 'vr', 'sni', 'spec']
        form_display = ['name', 'vr', 'sni', 'spec', 'hrange', 'house', 'patient']

    def __unicode__(self):
        full = []
        if self.name:
            full.append(unicode(self.name))
        if self.vr:
            full.append(self.vr)
        return u' '.join(full)


Index('ix_%s_name' % Division.__tablename__, func.lower(Division.name))
Index('ix_%s_vr' % Division.__tablename__, func.lower(Division.vr))


class PatientDivision(Entity):
    # temporary
    #__table_args__ = {'prefixes': ['TEMPORARY']}
    __tablename__ = 'patient_division'
    patient_id = Column(BigInteger(), primary_key=True)  # ManyToOne('Patient', required=True)
    division_id = Column(BigInteger(), primary_key=True)  # ManyToOne('Division', required=True)


################################################################################


class Diagnose(Entity):
    __tablename__ = 'diagnose'
    patient = OneToMany('Patient')
    pl = Column(Unicode(1))
    kod_mkb = Column(Unicode(6))
    name = Column(Unicode(128))
    zv = Column(Unicode(1))
    otmetka = Column(Boolean())
    otm1 = Column(Boolean())
    mkb = Column(Unicode(6))
    tcod = Column(Unicode(6))
    pravka = Column(Unicode(1))
    kcg1 = Column(Unicode(9))
    kcg2 = Column(Unicode(9))
    kcg3 = Column(Unicode(9))
    kcg4 = Column(Unicode(9))
    kcg5 = Column(Unicode(9))
    new = Column(Boolean())
    del_ = Column(Boolean())
    da_kcg = Column(Boolean())
    tr_s = Column(Unicode(2))
    tr_s_2 = Column(Unicode(2))

    @ColumnProperty
    def kod_f(self):
        return sql.func.substring(self.kod_mkb, 1, 1)

    class Admin(EntityAdmin):
        verbose_name = _('Diagnose')
        verbose_name_plural = _('Diagnoses')
        list_filter = [EditorFilter('kod_mkb'), ComboBoxFilter('kod_f')]
        list_display = ['kod_mkb', 'name', 'zv', 'otmetka', 'otm1', 'mkb', 'tcod', 'pravka', 'kcg1', 'kcg2', 'kcg3',
                        'kcg4', 'kcg5', 'new', 'del_', 'da_kcg', 'tr_s', 'tr_s_2']

    def __unicode__(self):
        full = []
        if self.kod_mkb:
            full.append(self.kod_mkb)
        if self.name:
            full.append(self.name)
        return u' '.join(full)


Index('ix_%s_kod_mkb' % Diagnose.__tablename__, Diagnose.kod_mkb)
Index('ix_%s_name' % Diagnose.__tablename__, func.lower(Diagnose.name))

trigger = DDL('''
    CREATE OR REPLACE FUNCTION diagnose_trigg_correct_func()
      RETURNS trigger AS
    $BODY$
    begin
      NEW.kod_mkb = upper(NEW.kod_mkb);
    return new;
    end;
    $BODY$
      LANGUAGE plpgsql VOLATILE;

    CREATE TRIGGER trigg_correct
    BEFORE INSERT OR UPDATE ON "diagnose"
    FOR EACH ROW
    EXECUTE PROCEDURE diagnose_trigg_correct_func();
''')

event.listen(
    Diagnose.table,
    'after_create',
    trigger.execute_if(dialect='postgresql')
)


class PatientDBF(Entity):
    # temporary
    #__table_args__ = {'prefixes': ['TEMPORARY']}
    __tablename__ = 'dbf_patient'

    n5 = Column(Unicode(5))
    n6 = Column(Unicode(16))
    n9 = Column(Unicode(2))
    n10 = Column(Unicode(3))
    n13 = Column(Unicode(13))
    n14 = Column(Unicode(17))
    n15 = Column(Unicode(36))
    n16 = Column(Unicode(20))
    n17 = Column(Unicode(3))
    n27 = Column(Unicode(10))
    fam = Column(Unicode(35))
    im = Column(Unicode(35))
    ot = Column(Unicode(35))
    drd = Column(Date())
    pol = Column(Unicode(1))
    snils = Column(Unicode(15))
    smo_cod = Column(BigInteger())
    cla_end = Column(Unicode(40))
    ss_end = Column(Unicode(40))
    cla_soc = Column(Unicode(10))
    str_end = Column(Unicode(40))
    str_soc = Column(Unicode(10))
    dom = Column(Unicode(10))
    numb = Column(BigInteger())
    kv = Column(Unicode(10))
    naiden = Column(Unicode(3))
    # gs = Column(Unicode(1))
    # lpu_cod = Column(Unicode(10))
    # lpu = Column(Unicode(2))
    lnkd = Column(Date())
    unld = Column(Date())
    skv_uch = Column(Unicode(8))
    adres = Column(Unicode(65))

    class Admin(EntityAdmin):
        verbose_name = _('Patient')
        verbose_name_plural = _('Patients')
        list_display = ['n5', 'n6', 'n9', 'n10', 'n13', 'n14', 'n15', 'n16', 'n17', 'n27', 'fam', 'im', 'ot', 'drd',
                        'pol', 'snils', 'smo_cod', 'cla_end', 'ss_end', 'cla_soc', 'str_end', 'str_soc', 'dom', 'numb',
                        'kv', 'naiden', 'unld', 'lnkd', 'skv_uch', 'adres']
        
        
Index('ix_%s_n5' % PatientDBF.__tablename__, PatientDBF.n5)
Index('ix_%s_n6' % PatientDBF.__tablename__, PatientDBF.n6)
Index('ix_%s_fam' % PatientDBF.__tablename__, func.lower(PatientDBF.fam))
Index('ix_%s_im' % PatientDBF.__tablename__, func.lower(PatientDBF.im))
Index('ix_%s_ot' % PatientDBF.__tablename__, func.lower(PatientDBF.ot))
Index('ix_%s_drd' % PatientDBF.__tablename__, PatientDBF.drd)
Index('ix_%s_snils' % PatientDBF.__tablename__, PatientDBF.snils)


class Patient(Entity):
    # using_options(order_by=['fam', 'im', 'ot', 'drd'])  # too slow
    __tablename__ = 'patient'

    house = ManyToOne('House', required=False, backref='patient', ondelete='set null')
    division = ManyToMany('Division',
                          tablename='patient_division__division_patient',
                          local_colname='patient_id',
                          remote_colname='division_id',
                          ondelete='cascade')  # , order_by=['spec', 'name', 'vr']
    mkb = ManyToOne('Diagnose', required=False, backref='patient', ondelete='set null')
    n5 = Column(Unicode(5))
    n6 = Column(Unicode(16))
    n9 = Column(Unicode(2))
    n10 = Column(Unicode(3))
    n13 = Column(Unicode(13))
    n14 = Column(Unicode(17))
    n15 = Column(Unicode(36))
    n16 = Column(Unicode(20))
    n17 = Column(Unicode(3))
    n27 = Column(Unicode(10))
    fam = Column(Unicode(35))
    im = Column(Unicode(35))
    ot = Column(Unicode(35))
    drd = Column(Date())
    pol = Column(Unicode(1))
    snils = Column(Unicode(15))
    smo_cod = Column(BigInteger())
    cla_end = Column(Unicode(40))
    ss_end = Column(Unicode(40))
    cla_soc = Column(Unicode(10))
    str_end = Column(Unicode(40))
    str_soc = Column(Unicode(10))
    dom = Column(Unicode(10))
    numb = Column(BigInteger())
    kv = Column(Unicode(10))
    # gs = Column(Unicode(9), default=u'G')
    naiden = Column(Unicode(3))
    # gs = Column(Unicode(1))
    # lpu_cod = Column(Unicode(10))
    # lpu = Column(Unicode(2))
    lnkd = Column(Date())
    unld = Column(Date())
    skv_uch = Column(Unicode(8))
    adres = Column(Unicode(65))

    @ColumnProperty
    def fam_f(self):
        return sql.func.substring(self.fam, 1, 2)

    @ColumnProperty
    def im_f(self):
        return sql.func.substring(self.im, 1, 2)

    @ColumnProperty
    def ot_f(self):
        return sql.func.substring(self.ot, 1, 2)

    class Admin(EntityAdmin):
        verbose_name = _('Patient')
        verbose_name_plural = _('Patients')
        list_filter = [EditorFilter('fam'), EditorFilter('im'), EditorFilter('ot'), ComboBoxFilter('fam_f'),
                       ComboBoxFilter('im_f'), ComboBoxFilter('ot_f'), EditorFilter('cla_end'), EditorFilter('str_end'),
                       ComboBoxFilter('mkb.name'), ComboBoxFilter('division.name'), ComboBoxFilter('pol')]
        list_search = ['n6', 'fam', 'im', 'ot', 'drd', 'snils', 'cla_end', 'ss_end', 'str_end', 'adres']
        field_attributes = {'n5': {'name': u'Полиса серия'},
                            'n6': {'name': u'Полиса номер'},
                            'house': {'name': u'Адрес КЛАДР'},
                            'division': {'name': u'Участок'},
                            'fam': {'name': u'Фамилия'},
                            'im': {'name': u'Имя'},
                            'ot': {'name': u'Отчество'},
                            'fam_f': {'name': u'Фамилия 2'},
                            'im_f': {'name': u'Имя 2'},
                            'ot_f': {'name': u'Отчество 2'},
                            'drd': {'name': u'Дата рождения'},
                            'pol': {'name': u'Пол',
                                    'choices': lambda o: [(u'м', _('Male')),
                                                          (u'ж', _('Female')),
                                                          (u'', _('Absent'))]},
                            'snils': {'name': u'СНИЛС'},
                            'mkb': {'name': u'Диагноз основной'},
                            'cla_end': {'name': u'Н.Пункт'},
                            'cla_soc': {'name': u'Тип Н.П.'},
                            'ss_end': {'name': u'Сельсовет'},
                            'str_end': {'name': u'Улица'},
                            'str_soc': {'name': u'Тип улицы'},
                            'dom': {'name': u'Дом'},
                            'adres': {'name': u'Адрес неточный'},
                            'numb': {'name': u'Дом числ.'},
                            'kv': {'name': u'Квартира'},
                            'unld': {'name': u'Откреплён'},
                            'lnkd': {'name': u'Прикреплён'}
                            }
        list_display = table.Table([
            table.ColumnGroup(_('General'),
                              ['n5', 'n6', 'fam', 'im', 'ot', 'drd', 'pol', 'snils', 'mkb', 'ss_end', 'cla_soc',
                               'cla_end', 'str_soc', 'str_end', 'dom', 'adres', 'unld', 'lnkd', 'house', 'division']),
            table.ColumnGroup(_('All'),
                              ['n5', 'n6', 'n9', 'n10', 'n13', 'n14', 'n15', 'n16', 'n17', 'n27', 'fam', 'im', 'ot',
                               'drd', 'pol', 'snils', 'mkb', 'smo_cod', 'ss_end', 'cla_soc', 'cla_end', 'str_soc',
                               'str_end', 'dom', 'numb', 'kv', 'naiden', 'unld', 'lnkd', 'skv_uch', 'adres', 'house',
                               'division'])])
        # list_action = OpenFormEdit()
        list_actions = [requests.AppendToRequestAction(), requests.AppendAllToRequestAction()]
        # , shform.AssignShapeAction(),
        #            shform.SetupByShapeAction()]
        form_display = ['n5', 'n6', 'fam', 'im', 'ot', 'drd', 'pol', 'snils', 'mkb', 'cla_end', 'cla_soc', 'ss_end',
                        'str_end', 'str_soc', 'dom', 'adres', 'house', 'division']

    def __unicode__(self):
        full = []
        if self.fam:
            full.append(self.fam)
        if self.im:
            full.append(self.im)
        if self.ot:
            full.append(self.ot)
        if self.drd:
            full.append(unicode(self.drd))
        return u' '.join(full)
    

Index('ix_%s_n5' % Patient.__tablename__, Patient.n5)
Index('ix_%s_n6' % Patient.__tablename__, Patient.n6)
Index('ix_%s_fam' % Patient.__tablename__, func.lower(Patient.fam))
Index('ix_%s_im' % Patient.__tablename__, func.lower(Patient.im))
Index('ix_%s_ot' % Patient.__tablename__, func.lower(Patient.ot))
Index('ix_%s_drd' % Patient.__tablename__, Patient.drd)
Index('ix_%s_snils' % Patient.__tablename__, Patient.snils)
Index('ix_%s_cla_end' % Patient.__tablename__, func.lower(Patient.cla_end))
Index('ix_%s_ss_end' % Patient.__tablename__, func.lower(Patient.ss_end))
Index('ix_%s_cla_soc' % Patient.__tablename__, func.lower(Patient.cla_soc))
Index('ix_%s_str_end' % Patient.__tablename__, func.lower(Patient.str_end))
Index('ix_%s_str_soc' % Patient.__tablename__, func.lower(Patient.str_soc))
Index('ix_%s_dom' % Patient.__tablename__, func.lower(Patient.dom))
Index('ix_%s_numb' % Patient.__tablename__, Patient.numb)
Index('ix_%s_kv' % Patient.__tablename__, func.lower(Patient.kv))

trigger = DDL('''
    CREATE OR REPLACE FUNCTION patient_trigg_null_func()
      RETURNS trigger AS
    $BODY$
    begin
      %s
    return new;
    end;
    $BODY$
      LANGUAGE plpgsql VOLATILE;

    CREATE TRIGGER trigg_null
    BEFORE INSERT OR UPDATE ON "patient"
    FOR EACH ROW
    EXECUTE PROCEDURE patient_trigg_null_func();
''' % ''.join('''if (NEW.%s = '') then
  NEW.%s = NULL;
end if;
''' % (i, i) for i in
       ('n5', 'n6', 'n9', 'n10', 'n13', 'n14', 'n15', 'n16', 'n17', 'n27', 'fam', 'im', 'ot', 'pol', 'snils',
        'ss_end', 'cla_soc', 'cla_end', 'str_soc', 'str_end', 'dom', 'kv', 'naiden', 'skv_uch', 'adres')))

event.listen(
    Patient.table,
    'after_create',
    trigger.execute_if(dialect='postgresql')
)


################################################################################


class RequestHdr(Entity):
    using_options(order_by=['-date', '-counter', '-type'])
    __tablename__ = 'request_hdr'

    record = OneToMany('VerrequestEntity')
    date = Column(Date)
    time = Column(Time)
    type = Column(BigInteger)
    counter = Column(BigInteger)

    class Admin(EntityAdmin):
        verbose_name = _('Request')
        verbose_name_plural = _('Requests')
        list_display = ['date', 'time', 'type', 'counter']
        list_actions = [requests.ProduceRequestAction(),
                        requests.LoadAnswerAction(),
                        requests.UpdateByAnswerAction(),
                        requests.DeleteRequestAction()]
        form_display = ['date', 'time', 'type', 'counter', 'record']
        
        
Index('ix_%s_date' % RequestHdr.__tablename__, RequestHdr.date)
Index('ix_%s_time' % RequestHdr.__tablename__, RequestHdr.time)
Index('ix_%s_type' % RequestHdr.__tablename__, RequestHdr.type)


class SimplePatient(Entity):
    # temporary
    #__table_args__ = {'prefixes': ['TEMPORARY']}
    __tablename__ = 'simple_pat'

    n5 = Column(Unicode(5))
    n6 = Column(Unicode(16))
    fam = Column(Unicode(35))
    im = Column(Unicode(35))
    ot = Column(Unicode(35))
    drd = Column(Date())
    pol = Column(Unicode(1))
    snils = Column(Unicode(15))
    smo_cod = Column(BigInteger())
    cla_end = Column(Unicode(40))
    ss_end = Column(Unicode(40))
    cla_soc = Column(Unicode(10))
    str_end = Column(Unicode(40))
    str_soc = Column(Unicode(10))
    dom = Column(Unicode(10))
    numb = Column(BigInteger())
    kv = Column(Unicode(10))
    # gs = Column(Unicode(9), default=u'G')
    naiden = Column(Unicode(3))
    # gs = Column(Unicode(1))
    # lpu_cod = Column(Unicode(10))
    # lpu = Column(Unicode(2))
    skv_uch = Column(Unicode(8))
    adres = Column(Unicode(65))

    class Admin(EntityAdmin):
        verbose_name = _('Simple patient')
        verbose_name_plural = _('Simple patients')
        field_attributes = {'fam': {'name': u'Фамилия'},
                            'im': {'name': u'Имя'},
                            'ot': {'name': u'Отчество'},
                            'drd': {'name': u'Дата рождения'},
                            'pol': {'name': u'Пол',
                                    'choices': lambda o: [(u'м', _('Male')),
                                                          (u'ж', _('Female')),
                                                          (u'', _('Absent'))]},
                            'snils': {'name': u'СНИЛС'},
                            'mkb': {'name': u'Диагноз основной'},
                            'cla_end': {'name': u'Н.Пункт'},
                            'cla_soc': {'name': u'Тип Н.П.'},
                            'ss_end': {'name': u'Сельсовет'},
                            'str_end': {'name': u'Улица'},
                            'str_soc': {'name': u'Тип улицы'},
                            'dom': {'name': u'Дом'},
                            'adres': {'name': u'Адрес неточный'},
                            'numb': {'name': u'Дом числ.'},
                            'kv': {'name': u'Квартира'}
                            }
        list_display = ['fam', 'im', 'ot', 'drd', 'pol', 'snils', 'smo_cod', 'ss_end', 'cla_soc', 'cla_end',
                        'str_soc', 'str_end', 'dom', 'numb', 'kv', 'naiden', 'skv_uch', 'adres']

    def __unicode__(self):
        full = []
        if self.fam:
            full.append(self.fam)
        if self.im:
            full.append(self.im)
        if self.ot:
            full.append(self.ot)
        if self.drd:
            full.append(unicode(self.drd))
        return u' '.join(full)
    
    
Index('ix_%s_n5' % SimplePatient.__tablename__, SimplePatient.n5)
Index('ix_%s_n6' % SimplePatient.__tablename__, SimplePatient.n6)
Index('ix_%s_fam' % SimplePatient.__tablename__, func.lower(SimplePatient.fam))
Index('ix_%s_im' % SimplePatient.__tablename__, func.lower(SimplePatient.im))
Index('ix_%s_ot' % SimplePatient.__tablename__, func.lower(SimplePatient.ot))
Index('ix_%s_drd' % SimplePatient.__tablename__, SimplePatient.drd)
Index('ix_%s_snils' % SimplePatient.__tablename__, SimplePatient.snils)
Index('ix_%s_cla_end' % SimplePatient.__tablename__, func.lower(SimplePatient.cla_end))
Index('ix_%s_ss_end' % SimplePatient.__tablename__, func.lower(SimplePatient.ss_end))
Index('ix_%s_cla_soc' % SimplePatient.__tablename__, func.lower(SimplePatient.cla_soc))
Index('ix_%s_str_end' % SimplePatient.__tablename__, func.lower(SimplePatient.str_end))
Index('ix_%s_str_soc' % SimplePatient.__tablename__, func.lower(SimplePatient.str_soc))
Index('ix_%s_numb' % SimplePatient.__tablename__, SimplePatient.numb)

trigger = DDL('''
    CREATE OR REPLACE FUNCTION simple_pat_trigg_null_func()
      RETURNS trigger AS
    $BODY$
    begin
      %s
    return new;
    end;
    $BODY$
      LANGUAGE plpgsql VOLATILE;

    CREATE TRIGGER trigg_null
    BEFORE INSERT OR UPDATE ON "simple_pat"
    FOR EACH ROW
    EXECUTE PROCEDURE simple_pat_trigg_null_func();
''' % ''.join('''if (NEW.%s = '') then
  NEW.%s = NULL;
end if;
''' % (i, i) for i in
       ('n5', 'n6', 'n9', 'n10', 'n13', 'n14', 'n15', 'n16', 'n17', 'n27', 'fam', 'im', 'ot', 'pol', 'snils',
        'ss_end', 'cla_soc', 'cla_end', 'str_soc', 'str_end', 'dom', 'kv', 'naiden', 'skv_uch', 'adres')))

event.listen(
    SimplePatient.table,
    'after_create',
    trigger.execute_if(dialect='postgresql')
)


class KeysList(Entity):
    #__table_args__ = {'prefixes': ['TEMPORARY']}
    __tablename__ = 'keys_list'

    id = Column(BigInteger(), index=True)


Index('ix_%s_id' % KeysList.__tablename__, KeysList.id)


################################################################################

temporaries = \
    [i
     for i in sys.modules[__name__].__dict__.values()
     if (isclass(i)
         and issubclass(i, Entity)
         and hasattr(i, '__table_args__')
         and 'TEMPORARY' in i.__table_args__.get('prefixes', []))]
autogenerated = OrderedDict(iter_specs(specs_all))
sys.modules[__name__].__dict__.update(autogenerated)

VerrequestEntity.header = ManyToOne('RequestHdr', required=True, backref='record', ondelete='restrict')
VerrequestEntity.patient = ManyToOne('Patient', required=False, backref='record', ondelete='set null')
VerrequestEntity.Admin.list_display.append('patient_id')

################################################################################

import shform

Patient.Admin.list_actions.extend(
    [shform.AssignShapeAction(), shform.SetupByShapeAction()]
)
