# -*- coding: utf-8 -*-

from camelot.admin.action import Action
from camelot.core.utils import ugettext_lazy as _
from camelot.core.orm import setup_all
from sqlalchemy.ext import compiler
from sqlalchemy.sql import select, insert, delete, update, join, outerjoin, func, and_, or_, expression, case
from model import Diagnose
#from utils import iter_dbf


class ImportDiagnoses(Action):
    verbose_name = _('Import Diagnoses')

    def model_run(self, model_context):
        from camelot.view.action_steps import (SelectFile,
                                               UpdateProgress,
                                               Refresh,
                                               FlushSession)
        select_archives = SelectFile('All Files (*)')
        select_archives.single = True
        file_names = yield select_archives
        print 'none'
        if not file_names:
            return

        import dbf
        from sqlalchemy import orm
        from camelot.core.orm import Session
        from camelot.core.sql import metadata
        from camelot.core.conf import settings
        from camelot.view.action_steps import MessageBox
        from sqlalchemy.sql import delete
        from sqlalchemy.exc import OperationalError
        from utils import iter_dbf
        from camelot.core.qt import QtGui

        def clean_stat(session):
            seq = (delete(Diagnose.table),
                   )
            for i in seq:
                session.execute(i)

        # session = Session()
        session = model_context.session

        with session.begin():
            clean_stat(session)

            def mk_pint(val, alt=''):
                ival = -1
                try:
                    ival = int(val.replace(val.lstrip('1234567890'), ''))
                except ValueError:
                    pass
                try:
                    if ival < 1:
                        ival = int(alt.replace(alt.lstrip('1234567890'), ''))
                except ValueError:
                    pass
                return ival

            for i, r in iter_dbf(file_names[0]):
                p = Diagnose()
                p.pl = r.pl
                p.kod_mkb = r.kod_mkb
                p.name = r.name
                p.zv = r.zv
                p.otmetka = r.otmetka
                p.otm1 = r.otm1
                p.mkb = r.mkb
                p.tcod = r.tcod
                p.pravka = r.pravka
                p.kcg1 = r.kcg1
                p.kcg2 = r.kcg2
                p.kcg3 = r.kcg3
                p.kcg4 = r.kcg4
                p.kcg5 = r.kcg5
                p.new = r.new
                # p.del_ = r.del_
                p.da_kcg = r.da_kcg
                p.tr_s = r.tr_s
                p.tr_s_2 = r.tr_s_2
                if i % 200 == 0:
                    yield FlushSession(session)

                yield FlushSession(model_context.session)

        yield Refresh()
