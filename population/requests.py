# -*- coding: utf-8 -*-

import os
import sys
import re
from datetime import datetime, date

from camelot.admin.action import Action
from camelot.core.utils import ugettext_lazy as _
from camelot.view.action_steps import FlushSession, Refresh
from sqlalchemy import sql, BigInteger, Date

import model
from specs import specs_all, dbf_by_spec, from_dbf_by_spec, get_spec_val

MO_CODE = 'BB'
TYPE_REQ = 1
TYPE_ANSW = 2
HOUSE = re.compile(r'(\d+)(.*)', re.U + re.I)


def var_attr_get(model, foreign, field):
    if hasattr(model, foreign):
        val = getattr(getattr(model, foreign), 'field', None)
    else:
        val = getattr(model, '%s_%s' % (foreign, field), None)
    # if val is None:
    #     print >> sys.stderr, 'WARNING: Using None for "%s.%s.%s"' % (model, foreign, field)
    return val


class AppendToRequestAction(Action):
    verbose_name = _('Append to check request')
    from specs import specs_all

    def model_run(self, model_context):

        korplen = get_spec_val('VerRequest', 'KORPUS').typ.size

        session = model_context.session
        today = date.today()

        with session.begin():
            for i, patient in enumerate(model_context.get_selection(), start=1):
                r = model.VerrequestEntity()
                r.header_id = 1
                r.patient_id = patient.id
                r.FAM = patient.fam
                r.IM = patient.im
                if patient.ot:
                    r.OTCH = patient.ot
                else:
                    r.D_TYPE = u'15'
                r.DR = patient.drd
                r.DBEG = today
                if patient.n6 and len(patient.n6) == 16:
                    r.POL_TIP = 3
                    r.OMS_NUM = patient.n6
                elif patient.n5 and len(patient.n5) <= 10 and len(patient.n6) <= 20:
                    r.POL_TIP = 1
                    r.SPOLIS = patient.n5
                    r.NPOLIS = patient.n6
                elif patient.n6 and len(patient.n6) <= 9:
                    r.POL_TIP = 2
                    r.TMP_NUM = patient.n6
                else:
                    print >> sys.stderr, 'WARNING: polis format "%s %s" (%s %s %s %s)' % \
                                         (patient.n5,
                                          patient.n6,
                                          patient.fam,
                                          patient.im,
                                          patient.ot,
                                          patient.drd)
                r.REGION_ID = var_attr_get(patient, 'house', 'codes')
                r.INDEKS = var_attr_get(patient, 'house', 'index')
                r.PUNKT = patient.cla_end
                r.ULICA = patient.str_end
                mo = HOUSE.search(patient.dom or '')
                if mo:
                    r.HOUSE = mo.group(1)
                    if mo.group(2) and len(mo.group(2)) <= korplen:
                        r.KORPUS = mo.group(2).upper()
                r.APARTMENTS = patient.kv
                if i % 200 == 0:
                    yield FlushSession(session)
            yield FlushSession(session)

        yield Refresh()


class AppendOrphanedToRequestAction(Action):
    verbose_name = _('Append orphaned to check request')

    def model_run(self, model_context):
        from sqlalchemy.sql import select, func, and_, or_, expression, case

        session = model_context.session
        today = date.today()

        pdtable = model.Patient.metadata.tables['patient_division__division_patient']

        with session.begin():
            q = select(
                [case([(model.Patient.n5.isnot(None), model.Patient.n5)]).label('SPOLIS'),
                 case([(model.Patient.n5.isnot(None), model.Patient.n6)]).label('NPOLIS'),
                 model.Patient.fam.label('FAM'),
                 model.Patient.im.label('IM'),
                 model.Patient.ot.label('OTCH'),
                 model.Patient.drd.label('DR'),
                 case([(model.Patient.ot.is_(None),
                        expression.literal(u'15'))],
                      else_=expression.literal('')).label('D_TYPE'),
                 expression.literal(today.strftime('%Y-%m-%d')).label('DBEG'),
                 case([(model.Patient.n5.is_(None),
                        case([(func.len(model.Patient.n6) == 16,
                               expression.literal('3'))],
                             else_=expression.literal('2')))],
                      else_=expression.literal('1')).label('POL_TIP'),
                 case([(and_(model.Patient.n5.is_(None),
                             func.len(model.Patient.n6) == 16), model.Patient.n6)]).label('OMS_NUM'),
                 case([(and_(model.Patient.n5.is_(None),
                             func.len(model.Patient.n6) != 16),
                        func.substring(model.Patient.n6, 1, 9))]).label('TMP_NUM'),
                 expression.literal('1', type_=BigInteger()).label('header_id'),
                 model.Patient.id.label('patient_id')
                 ],
                whereclause=and_(pdtable.c.patient_id.is_(None), model.Patient.house_id.is_(None))) \
                .select_from(model.Patient.table.outerjoin(pdtable)) \
                .alias()

            q = model.VerrequestEntity.table.insert(inline=True).from_select(
                ['SPOLIS', 'NPOLIS', 'FAM', 'IM', 'OTCH', 'DR', 'DBEG', 'POL_TIP', 'OMS_NUM', 'TMP_NUM', 'header_id',
                 'patient_id'],
                q)
            session.execute(q)
            yield FlushSession(session)

        yield Refresh()


class AppendAllToRequestAction(Action):
    verbose_name = _('Append all to check request')

    def model_run(self, model_context):

        korplen = get_spec_val('VerRequest', 'KORPUS').typ.size

        session = model_context.session
        today = date.today()

        with session.begin():
            for i, patient in enumerate(model_context.get_collection(yield_per=200), start=1):
                r = model.VerrequestEntity()
                r.header_id = 1
                r.patient_id = patient.id
                r.FAM = patient.fam
                r.IM = patient.im
                if patient.ot:
                    r.OTCH = patient.ot
                else:
                    r.D_TYPE = u'15'
                r.DR = patient.drd
                r.DBEG = today
                # r.SS = patient.snils
                r.SMO = unicode(patient.smo_cod or u'')
                if patient.n6 and len(patient.n6) == 16:
                    r.POL_TIP = 3
                    r.OMS_NUM = patient.n6
                elif patient.n5 and len(patient.n5) <= 10 and len(patient.n6) <= 20:
                    r.POL_TIP = 1
                    r.SPOLIS = patient.n5
                    r.NPOLIS = patient.n6
                elif patient.n6 and len(patient.n6) <= 9:
                    r.POL_TIP = 2
                    r.TMP_NUM = patient.n6
                else:
                    print >> sys.stderr, 'WARNING: polis format "%s %s" (%s %s %s %s)' % \
                                         (patient.n5,
                                          patient.n6,
                                          patient.fam,
                                          patient.im,
                                          patient.ot,
                                          patient.drd)
                r.REGION_ID = var_attr_get(patient, 'house', 'codes')
                r.INDEKS = var_attr_get(patient, 'house', 'index')
                r.PUNKT = patient.cla_end
                r.ULICA = patient.str_end
                mo = HOUSE.search(patient.dom or '')
                if mo:
                    r.HOUSE = mo.group(1)
                    if mo.group(2) and len(mo.group(2)) <= korplen:
                        r.KORPUS = mo.group(2).upper()
                r.APARTMENTS = patient.kv
                if i % 200 == 0:
                    yield FlushSession(session)
            yield FlushSession(session)

        yield Refresh()


class ProduceRequestAction(Action):
    verbose_name = _('Produce request file')

    def model_run(self, model_context):
        from camelot.core.qt import QtGui
        from sqlalchemy.sql import update, select, func, and_
        from camelot.view.action_steps import MessageBox
        from os import path

        session = model_context.session

        if not session.query(model.VerrequestEntity).filter(model.VerrequestEntity.header_id == 1).first():
            yield MessageBox(_(u'No data for requesting'),
                             QtGui.QMessageBox.Warning,
                             standard_buttons=QtGui.QMessageBox.Cancel)
            return

        with session.begin():
            now = datetime.now()
            q = select([sql.func.max(model.RequestHdr.counter).label('counter')],
                       whereclause=model.RequestHdr.date == now.date())
            counter = tuple(session.execute(q))[0][0]
            counter = (counter + 1) if counter else 1

            # Make new header
            request = model.RequestHdr()
            request.date = now.date()
            request.time = now.time()
            request.type = TYPE_REQ
            request.counter = counter

            yield FlushSession(session)  # Flush for key obtaining

            # Reassigning to new header and enumeration
            q = select(
                [model.VerrequestEntity.id,
                 func.row_number().over(order_by=model.VerrequestEntity.id).label('vr_counter')],
                whereclause=model.VerrequestEntity.header_id == 1).with_for_update().alias()

            q = update(
                model.VerrequestEntity,
                whereclause=and_(model.VerrequestEntity.header_id == 1,
                                 model.VerrequestEntity.id == q.c.id),
                values={
                    'NPR_Q': q.c.vr_counter,
                    'KVARTAL': sql.case(
                        [(and_(func.extract('month', model.VerrequestEntity.DR) >= 1,
                               func.extract('month', model.VerrequestEntity.DR) < 4),
                          sql.expression.literal(1)),
                         (and_(func.extract('month', model.VerrequestEntity.DR) >= 4,
                               func.extract('month', model.VerrequestEntity.DR) < 7),
                          sql.expression.literal(2)),
                         (and_(func.extract('month', model.VerrequestEntity.DR) >= 7,
                               func.extract('month', model.VerrequestEntity.DR) < 10),
                          sql.expression.literal(3))
                         ],
                        else_=sql.expression.literal(4)),
                    'header_id': request.id
                }
            )
            session.execute(q)
            yield FlushSession(session)
        yield Refresh()

        filename = 'J%s_%s%02d.DBF' % (MO_CODE, now.date().strftime('%d%m%y'), counter)
        fullname = path.expanduser('~/%s' % filename)
        dataset = (
            rec.__dict__
            for rec in
            session.query(model.VerrequestEntity).filter(model.VerrequestEntity.header_id == request.id))
        dbf_by_spec(fullname, 'VerRequest', specs_all, dataset)

        yield Refresh()


class LoadAnswerAction(Action):
    verbose_name = _("Load request's answer file")

    def model_run(self, model_context):
        from camelot.core.qt import QtGui
        from sqlalchemy.sql import delete, update, select, and_
        from camelot.view.action_steps import SelectFile, MessageBox

        select_archives = SelectFile('Answers (Q%s_????????.DBF);;All Files (*)' % MO_CODE)
        select_archives.single = True
        file_names = yield select_archives
        if not file_names:
            return
        file_name = file_names[0]

        session = model_context.session

        with session.begin():

            if session.query(model.VerrequestEntity).filter(model.VerrequestEntity.header_id == 1).first():
                yield MessageBox(_(u'Have unprocessed data'), QtGui.QMessageBox.Warning,
                                 standard_buttons=QtGui.QMessageBox.Cancel)
                return

            for event in from_dbf_by_spec(file_name, session, 'VerRequest', defaults={'header_id': 1}):
                yield event

            if not session.query(model.VerrequestEntity).filter(model.VerrequestEntity.header_id == 1).first():
                yield MessageBox(_(u'No data loaded'), QtGui.QMessageBox.Warning,
                                 standard_buttons=QtGui.QMessageBox.Cancel)
                return

            counter = int(os.path.basename(file_name)[10:12])
            dbeg = session.query(model.VerrequestEntity).filter(model.VerrequestEntity.header_id == 1).first().DBEG

            sel_cond = and_(model.RequestHdr.date == dbeg,
                            model.RequestHdr.type == TYPE_ANSW,
                            model.RequestHdr.counter == counter)
            q = delete(
                model.VerrequestEntity,
                whereclause=model.VerrequestEntity.header_id.in_(
                    select([model.RequestHdr.id], whereclause=sel_cond)))
            session.execute(q)
            q = delete(model.RequestHdr, whereclause=sel_cond)
            session.execute(q)

            # Set patients keys

            q_r = select(
                [model.VerrequestEntity],
                whereclause=model.VerrequestEntity.header_id.in_(
                    select([model.RequestHdr.id],
                           whereclause=and_(
                               model.RequestHdr.date == dbeg,
                               model.RequestHdr.type == TYPE_REQ,
                               model.RequestHdr.counter == counter)))).with_for_update().alias()

            q = update(
                model.VerrequestEntity,
                whereclause=and_(
                    model.VerrequestEntity.header_id == 1,
                    model.VerrequestEntity.NPR_Q == q_r.c.NPR_Q
                ),
                values={'patient_id': q_r.c.patient_id})
            session.execute(q)

            # Make new header
            request = model.RequestHdr()
            request.date = dbeg
            request.type = TYPE_ANSW
            request.counter = counter

            yield FlushSession(session)  # Flush for key obtaining

            # Reassigning to new header
            q = update(model.VerrequestEntity,
                       whereclause=model.VerrequestEntity.header_id == 1,
                       values={'header_id': request.id})
            session.execute(q)

            yield FlushSession(session)

        yield Refresh()


class UpdateByAnswerAction(Action):
    verbose_name = _("Update data by answer")

    def model_run(self, model_context):
        from sqlalchemy.sql import update, func, and_

        def val_new_cond(oattr, nattr, old, new):
            return sql.case([(getattr(new, nattr) != None, getattr(new, nattr))], else_=getattr(old, oattr))

        def extract(field, errcode):
            return sql.case(
                [(and_(
                    model.VerrequestEntity.SVREG == errcode,
                    func.patindex(u'%: %', model.VerrequestEntity.COMMENTS) > 0
                ), func.ltrim(func.rtrim(func.right(model.VerrequestEntity.COMMENTS,
                                                    func.len(model.VerrequestEntity.COMMENTS) -
                                                    func.patindex(u'%: %', model.VerrequestEntity.COMMENTS) - 1))))],
                else_=field)

        def extract_date(errcode):
            return sql.case(
                [(and_(
                    model.VerrequestEntity.SVREG == errcode,
                    func.patindex(u'%: %', model.VerrequestEntity.COMMENTS) > 0
                ), func.ltrim(func.rtrim(func.right(model.VerrequestEntity.COMMENTS,
                                                    func.len(model.VerrequestEntity.COMMENTS) -
                                                    func.patindex(u'%: %', model.VerrequestEntity.COMMENTS) - 1)))
                      .cast(Date))],
                else_=model.VerrequestEntity.DR)

        session = model_context.session

        with session.begin():
            current = model_context.get_object()
            if current is None or current.type != TYPE_ANSW:
                return

            q = update(
                model.Patient,
                whereclause=and_(
                    model.Patient.id == model.VerrequestEntity.patient_id,
                    model.VerrequestEntity.header_id == current.id
                ),
                values={
                    'smo_cod': sql.case(
                        [(and_(model.VerrequestEntity.SVREG != 17, model.VerrequestEntity.SMO != None),
                          model.VerrequestEntity.SMO.cast(BigInteger)),
                         (and_(model.VerrequestEntity.SVREG == 17),
                          sql.expression.literal(-1))
                         ],
                        else_=model.Patient.smo_cod),
                    'snils': val_new_cond(
                        'snils',
                        'SS',
                        model.Patient,
                        model.VerrequestEntity),
                    'fam': extract(model.Patient.fam, 13),
                    'im': extract(model.Patient.im, 14),
                    'ot': extract(model.Patient.ot, 15),
                    'drd': extract_date(16),
                    'n5': sql.case(
                        [(model.VerrequestEntity.POL_TIP == 1, model.VerrequestEntity.SPOLIS)],
                        else_=None),
                    'n6': sql.case(
                        [(model.VerrequestEntity.POL_TIP == 1, model.VerrequestEntity.NPOLIS),
                         (model.VerrequestEntity.POL_TIP == 2, model.VerrequestEntity.TMP_NUM)],
                        else_=model.VerrequestEntity.OMS_NUM)
                }
            )

            session.execute(q)
            yield FlushSession(session)

        yield Refresh()


class DeleteRequestAction(Action):
    verbose_name = _("Delete element")

    def model_run(self, model_context):
        from sqlalchemy.sql import delete
        from camelot.view.action_steps import MessageBox
        from camelot.core.qt import QtGui

        session = model_context.session

        with session.begin():
            current = model_context.get_object()
            if current is None:
                return

            yield MessageBox(_(u'Do you want to delete request?'), QtGui.QMessageBox.Question, title=_(u'confirmation'))

            q = delete(
                model.VerrequestEntity,
                whereclause=model.VerrequestEntity.header_id == current.id
            )
            session.execute(q)

            if current.id != 1:
                q = delete(model.RequestHdr, whereclause=model.RequestHdr.id == current.id)
                session.execute(q)

            yield FlushSession(session)

        yield Refresh()
