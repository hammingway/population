# -*- coding: utf-8 -*-

import sys
import re
from collections import namedtuple, OrderedDict
#from utils import mk_eng, mk_rus

_text = u'''
VerRequest  NPR_Q       У   N(7)	Уникальный порядковый номер записи внутри файла запроса/ответа
            NCARD	    У   T(17)	№ ист. болезни; № карты амб. пациента; № журнала учета консультаций; № карты стом. пациента; № журнала регистрации амб. больных
            SVREG	    У   N(2)	Результат поиска в регистре на дату обращения пациента в МО поле «DBEG»:
            SPOLIS	    У   T(10)	Серия полиса ОМС
            NPOLIS	    У   T(20)	Номер полиса ОМС
            SS	        У   T(14)	СНИЛС
            SMO	        У   T(3)	Код СМО (Приложение №11 к Положению об электронном обмене данными)
            DOC_TIP	    У   N(2)	Код вида документа, удостоверяющего личность (Приложение №17 к Положению об электронном обмене данными)
            DOC_SER	    У   T(10)	Серия паспорта, иного документа
            DOC_NUM	    У   T(10)	Номер паспорта, иного документа
            FAM	        О   T(40)	Фамилия
            IM	        О   T(40)	Имя
            OTCH	    У   T(40)	Отчество (не заполняется в случае отсутствия в документах, удостоверяющих личность пациента, при этом в поле №17 D_TYPE проставляется значение “15”)
            DR	        О   D   	Полная дата рождения (например, 12.02.2001)
            RAB	        У   N(1)	Статус: работает –1; не работает – 2;
            D_TYPE	    У   T(4)	Особый случай: в случае отсутствия у пациента отчества, в данное поле проставляется значение “15”, остальные значения игнорируются
            REGION_ID	У   T(2)	Код субъекта РФ (Приложение №18 к Положению об электронном обмене данными)
            RAION_ID	У   T(2)	Код района (Приложение №15 к Положению об электронном обмене данными)
            INDEKS	    У   T(6)	Индекс
            RAION	    У   T(40)	Район места постоянной регистрации:
            PUNKT	    У   T(63)	Населенный пункт места постоянной регистрации:
            ULICA	    У   T(63)	Название улицы
            HOUSE	    У   T(10)	Дом
            KORPUS	    У   T(5)	Корпус
            APARTMENTS	У   T(5)	Квартира
            DATE_IN	    У   T(16)	Дата и время получения запроса (в виде “21.01.2008 14:51”)
            DATE_OUT	У   T(16)	Дата и время отправки ответа (в виде “21.01.2008 14:51”)
            DATE_POL	У   D   	Дата выдачи полиса
            DATE_RZ	    У   D   	Дата актуализации сведений о застрахованных
            DBEG	    О   D   	Дата обращения пациента за медицинской помощью в МО
            COMMENTS	У   T(110)	Комментарии к результатам поиска
            DOC_DATE	У   D   	Дата выдачи документа
            OMS_NUMBER	У   T(17)	Номер ОМС
            TEMP	    У   T(50)	Резервное поле
            POL_TIP	    У   N(1)	Код типа документа, подтверждающего факт страхования по ОМС (Приложение №62 столбец 1)
            OMS_NUM	    У   T(16)	Единый номер полиса ОМС
            TMP_NUM	    У   T(9)	Номер временного свидетельства, подтверждающего оформление полиса ОМС
            LPU_PRIK	У   T(6)	Код МО прикрепления застрахованного лица (Приложение №9 к Положению об электронном обмене данными, поле «Код ЛПУ (ФОМС)»)
            KVARTAL     У   N(1)    Номер квартала планового проведения профилактических мероприятий
'''

TableSpec = namedtuple('TableSpec', ['to_one', 'fields', 'attribs'])
SpecVField = namedtuple('SpecVField', ['fld', 'spc', 'typ'])
SpecVType = namedtuple('SpecVType', ['id', 'size', 'prec'])


class DataValidityException(Exception):
    pass


def get_type(s):
    from utils import mk_eng
    fs = s.split('(')
    if len(fs) > 1:
        t, r = fs
    else:
        t, r = fs[0], ''
    if r:
        ss = map(int, r.rstrip(')').split('.'))
    else:
        ss = [0]
    return SpecVType(mk_eng(t), ss[0], ss[1] if len(ss) > 1 else 0)


def make_specs(text):
    from utils import mk_eng, mk_rus
    group = ''
    specs = OrderedDict()
    for line in re.sub('[ \t\f\v]+', ' ', text).strip().split('\n'):
        grp, fld, spc, typ = line.split(' ')[:4]
        grp = mk_eng(grp)
        if grp:
            group = grp
        else:
            grp = group
        fld = mk_eng(fld)
        spc = mk_rus(spc)
        typ = get_type(typ)
        specs.setdefault(grp, TableSpec([], [], {'is_node': False})).fields.append(SpecVField(fld, spc, typ))
    for grp, val in specs.items():
        for fld in val.fields:
            if fld.typ.id == 'S':
                specs[fld.fld].to_one.append(grp)
                specs[grp].attribs['is_node'] = True
    return specs


specs_all = make_specs(_text)


def table_name(name):
    return 'entity_%s' % name.lower()


def entity_name(name):
    return str('%sEntity' % name.title())


def entity_by_spec(name, specs, collation=None):
    from camelot.admin.entity_admin import EntityAdmin
    from sqlalchemy.schema import Column
    from sqlalchemy import Unicode, Date, Numeric, BigInteger
    from camelot.core.orm import Entity, ManyToOne

    def gen_fields(specs):
        for fld, spc, (id_, size, prec) in specs:
            kwargs = {}
            if fld.lower() == 'id':
                kwargs['primary_key'] = True
            if id_ == 'T':
                if collation is None:
                    col = Column(fld, Unicode(size), **kwargs)
                else:
                    col = Column(fld, Unicode(size, collation=collation), **kwargs)
            elif id_ == 'N':
                if prec:
                    col = Column(fld, Numeric(precision=size, scale=prec), **kwargs)
                else:
                    col = Column(fld, BigInteger, **kwargs)
            elif id_ == 'D':
                col = Column(fld, Date, **kwargs)
            elif id_ == 'S':
                continue
            else:
                raise DataValidityException('unknown type specification "%s" at field "%s"' % (id_, fld))
            yield fld, spc, col

    columns = OrderedDict((f, c) for f, _, c in gen_fields(specs.fields))
    the_dict = {'__tablename__': table_name(name),
                'Admin': type('Admin',
                              (EntityAdmin,),
                              {'verbose_name_plural': name,
                               'list_display': columns.keys()})}
    the_dict.update(columns)
    for grp in specs.to_one:
        the_dict[grp.lower()] = ManyToOne(entity_name(grp), required=False)
    return type(entity_name(name), (Entity,), the_dict)


def dbf_by_spec(filename, name, specs, dataset, translators=None):
    import dbf
    from operator import itemgetter

    def create_format(specs):

        def get_spec(id_, size, prec):
            if id_ == 'N':
                return 'N(%s,%s)' % (size, prec)
            elif id_ == 'T':
                return 'C(%s)' % size
            elif id_ == 'D':
                return 'D'

        return str(';'.join('%s %s' % (fld, get_spec(id_, size, prec))
                            for fld, spc, id_, size, prec in specs))

    def validate(val, fld):
        if fld in imp_fields and val is None:
            raise DataValidityException('None value for field [%s]' % fld)
        return val

    if translators is None:
        translators = {}
    fields = [
        (fld, spc, id_, size, prec)
        for fld, spc, (id_, size, prec) in specs[name].fields
        if id_ != 'S' and u'М' not in spc]
    used_fields = set(map(itemgetter(0), fields))
    imp_fields = set(fld for fld, spc, id_, size, prec in fields if u'О' in spc)
    tbl = dbf.Table(filename, create_format(fields), dbf_type='db3', codepage='cp866')
    tbl.open()
    try:
        for rec in dataset:
            tbl.append(
                {fld.lower(): validate(translators.get(fld, lambda x: x)(val), fld)
                 for fld, val in rec.iteritems()
                 if fld in used_fields})
    finally:
        tbl.close()


def from_dbf_by_spec(filename, session, name, translators=None, defaults=None):
    import dbf
    from camelot.view.action_steps import FlushSession
    import model

    cls = model.autogenerated[entity_name(name)]
    tbl = dbf.Table(filename, codepage='cp866')
    tbl.open()
    try:
        specs = {
            i.fld.lower(): i for i in model.specs_all[name].fields
            if i.typ.id != 'S' and u'М' not in i.spc}
        ofields = set(tbl._meta.fields)
        ifields = set(specs.keys())
        mfields = set(i.fld.lower() for i in model.specs_all[name].fields if u'О' in i.spc)
        outf = ifields ^ ofields
        absentf = mfields & outf
        if outf:
            print >> sys.stderr, 'WARNING: fields out of format [%s]' % ', '.join(sorted(outf))
        if absentf:
            raise DataValidityException('Expected presence of fields [%s]' % ', '.join(sorted(absentf)))
        for i, r in enumerate(tbl):
            if dbf.is_deleted(r):
                continue
            d = cls()
            for n, v in zip(tbl._meta.fields, r):
                if n not in ifields:
                    continue
                sp = specs[n]
                if v and sp.typ.id == 'T':
                    val = v.strip()
                    if not val:
                        val = None
                else:
                    val = v
                if translators and sp.fld in translators:
                    val = translators[sp.fld](val)
                if val is None and u'О' in sp.spc:
                    raise DataValidityException('None value for field [%s]' % sp.fld)
                setattr(d, sp.fld, val)
            if defaults:
                for k, v in defaults.iteritems():
                    setattr(d, k, v)
            if i % 200 == 0:
                yield FlushSession(session)
        yield FlushSession(session)
    finally:
        tbl.close()


def iter_specs(specs, collation=None):
    for n, v in specs.iteritems():
        yield entity_name(n), entity_by_spec(n, v, collation)


def get_spec_val(tbl, fld):
    return filter(lambda x: x.fld == fld, specs_all[tbl].fields)[0]


if __name__ == '__main__':
    # entity_by_spec('SCHET', specs['SCHET'])
    print specs_all['VerRequest']
