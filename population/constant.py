from camelot.admin.action import Action
from camelot.core.utils import ugettext_lazy as _
from camelot.admin.entity_admin import EntityAdmin
from camelot.core.orm.options import using_options
from camelot.admin import table
from model import RequestHdr


def update_fixtures():
    from camelot.model.fixture import Fixture

    Fixture.insert_or_update_fixture(
        RequestHdr,
        fixture_key=u'1',
        values=dict(counter=-1)
    )


    # class UpdateFixtures(Action):
    #     verbose_name = _('Refresh references')
    #
    #     def model_run(self, model_context):
    #         from camelot.view.action_steps import Refresh
    #
    #         update_fixtures()
    #         yield Refresh()
