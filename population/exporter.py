# -*- coding: utf-8 -*-

import datetime
import six
from camelot.core.utils import ugettext, ugettext_lazy as _
from camelot.view.art import Icon
from camelot.admin.action.list_action import (ListContextAction, SaveExportMapping, RestoreExportMapping,
                                              RemoveExportMapping)


class ExportXbase(ListContextAction):
    """Export all rows in a table to a xbase table"""

    icon = Icon('icons/16x16/dbf_16.png')
    tooltip = _('Export to xBase file')
    verbose_name = _('Export to xBase file')

    def model_run(self, model_context):
        import dbf
        from decimal import Decimal
        from operator import itemgetter
        from camelot.view.import_utils import (ColumnMapping,
                                               ColumnSelectionAdmin)
        from camelot.view import action_steps

        yield action_steps.UpdateProgress(text=_('Prepare export'))
        admin = model_context.admin
        settings = admin.get_settings()
        settings.beginGroup('export_xbase')
        all_fields = admin.get_all_fields_and_attributes()
        field_choices = [(f, six.text_type(entity_fa['name'])) for f, entity_fa in
                         six.iteritems(all_fields)]
        field_choices.sort(key=lambda field_tuple: field_tuple[1])
        row_data = [None] * len(all_fields)
        mappings = []
        used = set()
        i = 0
        for default_field in admin.get_columns():
            if default_field[0] not in used:
                mappings.append(ColumnMapping(i, [row_data], default_field[0]))
                used.add(default_field[0])
                i += 1

        mapping_admin = ColumnSelectionAdmin(admin, field_choices=field_choices)
        mapping_admin.related_toolbar_actions = [
            SaveExportMapping(settings),
            RestoreExportMapping(settings),
            RemoveExportMapping(settings)]
        change_mappings = action_steps.ChangeObjects(mappings, mapping_admin)
        change_mappings.title = _('Select field')
        change_mappings.subtitle = _('Specify for each column the field to export')
        yield change_mappings
        settings.endGroup()
        columns = []
        for i, mapping in enumerate(mappings):
            if mapping.field is not None:
                columns.append((mapping.field, all_fields[mapping.field]))

        yield action_steps.UpdateProgress(text=_('Create dbf'))

        def create_format(static_attributes):

            def get_spec(t, a):
                if issubclass(t, (Decimal, float)):
                    sz, pr = max(len(repr(a['maximum'])), len(repr(a['minimum']))) - 2 + a['precision'], a['precision']
                    if not pr:
                        return 'N(%s,0)' % sz
                    return 'F(%s,%s)' % (sz, pr)
                elif issubclass(t, six.string_types):
                    return 'C(%s)' % (a['length'] if a['length'] else 254)
                elif issubclass(t, list):
                    return 'C(254)'
                elif issubclass(t, (int, long)):
                    return 'N(%s,0)' % (max(len(repr(a['maximum'])), len(repr(a['minimum']))))
                elif issubclass(t, bool):
                    return 'L'
                elif issubclass(t, datetime.date):
                    return 'D'
                elif issubclass(t, datetime.datetime):
                    return 'C(14)'  # YYYYMMDDHHMMSS
                elif issubclass(t, datetime.time):
                    return 'C(6)'  # HHMMSS
                return 'C(1)'

            return str(
                ';'.join(
                    '%s %s' % (atr['field_name'].lower(), get_spec(atr['python_type'], atr))
                    for atr in static_attributes))

        def transl_val(val, attributes):
            if val is not None:
                if isinstance(
                        val, (Decimal,
                              float,
                              int,
                              long,
                              bool,
                              datetime.date)):
                    return val
                elif isinstance(val, list):
                    separator = attributes.get('separator', u', ')
                    return separator.join([six.text_type(el) for el in val])[:254]
                elif isinstance(val, datetime.datetime):
                    return val.strftime('%Y%m%d%H%M%S')
                elif isinstance(val, datetime.time):
                    return val.strftime('%H%M%S')
                return six.text_type(val)[:254]
            return None

        filename = action_steps.OpenFile.create_temporary_file('.dbf')
        field_names = map(itemgetter(0), columns)
        static_attributes = list(admin.get_static_field_attributes(field_names))
        # print create_format(static_attributes)
        tbl = dbf.Table(filename, create_format(static_attributes), dbf_type='db3', codepage='cp866')
        tbl.open()
        try:
            for j, obj in enumerate(model_context.get_collection(yield_per=100)):
                dynamic_attributes = admin.get_dynamic_field_attributes(obj,
                                                                        field_names)
                if j % 100 == 0:
                    yield action_steps.UpdateProgress(j, model_context.collection_count)
                fields = six.moves.zip(field_names, static_attributes, dynamic_attributes)
                tbl.append({name.lower(): transl_val(getattr(obj, name), attributes)
                            for name, attributes, delta_attributes in fields})
        finally:
            tbl.close()

        # yield action_steps.UpdateProgress(text=_('Saving file'))
        # filename = action_steps.OpenFile.create_temporary_file('.xls')
        yield action_steps.UpdateProgress(text=_('File opening'))
        yield action_steps.OpenFile(filename)
