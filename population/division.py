# -*- coding: utf-8 -*-

from camelot.admin.action import Action
from camelot.core.utils import ugettext_lazy as _
from camelot.core.orm import setup_all
from sqlalchemy.ext import compiler
from sqlalchemy.sql import select, insert, delete, join, outerjoin, func, and_, or_, expression, case, text
from model import Town, Street, House, DivisionDBF, Division, HouseDivision, HousesRange
#from utils import iter_dbf


class ImportDivisions(Action):
    verbose_name = _('Import Divisions')

    def model_run(self, model_context):
        from camelot.view.action_steps import (SelectFile,
                                               UpdateProgress,
                                               Refresh,
                                               FlushSession)
        import dbf
        from sqlalchemy import orm
        from camelot.core.orm import Session
        from camelot.core.sql import metadata
        from camelot.core.conf import settings
        from camelot.view.action_steps import MessageBox
        from sqlalchemy.sql import delete
        from sqlalchemy.exc import OperationalError
        from utils import mk_town, mk_street, mk_ces, mk_rus, mk_ss, is_town, iter_dbf
        from camelot.core.qt import QtGui

        select_archives = SelectFile('XBase Files (*.DBF);;All Files (*)')
        select_archives.single = True
        file_names = yield select_archives
        if not file_names:
            return

        def clean_stat(session):
            seq = (delete(HousesRange.table),
                   delete(DivisionDBF.table),
                   delete(HouseDivision.table),
                   delete(Division.metadata.tables['house_division__division_house']),
                   delete(Division.metadata.tables['patient_division__division_patient']),
                   delete(Division.table))
            for i in seq:
                session.execute(i)

        # session = Session()
        session = model_context.session

        with session.begin():
            clean_stat(session)

            def mk_pint(val):
                ival = -1
                try:
                    ival = int(val)
                except (ValueError, TypeError):
                    pass
                return ival

            for i, r in iter_dbf(file_names[0]):
                d = DivisionDBF()
                d.n = r.n
                d.okrug = mk_rus(r.okrug.upper())
                d.punkt_1 = mk_rus(r.punkt_1.upper())
                d.ulica_1 = mk_rus(r.ulica_1.upper())
                d.gs = r.gs.upper()
                vn = mk_pint(r.vse_n)
                ve = mk_pint(r.vse_e.replace(r.vse_e.lstrip('1234567890'), ''))
                nn = mk_pint(r.nech_n.replace(r.nech_n.lstrip('1234567890'), ''))
                ne = mk_pint(r.nech_e.replace(r.nech_e.lstrip('1234567890'), ''))
                kn = mk_pint(r.chet_n.replace(r.chet_n.lstrip('1234567890'), ''))
                ke = mk_pint(r.chet_e.replace(r.chet_e.lstrip('1234567890'), ''))
                d.vse_n = vn
                d.vse_e = ve
                d.nech_n = nn if nn > 0 else vn
                d.nech_e = ne if ne > 0 else (ve if ve >= d.nech_n else 10000)
                d.chet_n = kn if kn > 0 else vn
                d.chet_e = ke if ke > 0 else (ve if ve >= d.chet_n else 10000)
                d.spis = r.spis
                d.uch_vz = r.uch_vz.upper()
                d.vz_vr = r.vz_vr.upper()
                d.sni_vzr = r.sni_vzr
                d.uch_det = r.uch_det.upper()
                d.det_vr = r.det_vr.upper()
                d.sni_det = r.sni_det
                d.uch_ven = r.uch_ven.upper()
                d.cla_prom = mk_rus(r.cla_prom.upper())
                d.cladr = mk_rus(r.cladr.upper())
                st = mk_rus(r.street.upper())
                d.street = st
                ces = mk_ces(mk_rus(r.ss_end.upper()))
                se, ss = mk_rus(r.str_end.upper()), mk_rus(r.str_soc.upper())
                if not se:
                    se = st
                if is_town(se):
                    # print se
                    ce, cs, cec = mk_town(se, '', ces, True)
                    d.str_end = u''
                    d.str_soc = u''
                    d.name = ce
                else:
                    d.name = mk_rus(r.name.upper())
                    ce, cs = mk_rus(r.cla_end.upper()), mk_rus(r.cla_soc.upper())
                    ce, cs, cec = mk_town(ce, cs, ces, True)
                    se, ss = mk_street(se, ss)
                    d.str_end = se
                    ss = mk_ss(se, ss)
                    d.str_soc = ss
                d.cla_end = cec
                d.ss_end = ces
                d.cla_soc = cs
                d.cla_cod = mk_rus(r.cla_cod.upper())
                d.str_cod = mk_rus(r.str_cod.upper())
                if i % 200 == 0:
                    yield FlushSession(session)

            yield FlushSession(session)

            q = select(
                [DivisionDBF.uch_vz.label('name'),
                 DivisionDBF.vz_vr.label('vr'),
                 DivisionDBF.sni_vzr.label('sni'),
                 expression.literal(u'взрослые').label('spec')],
                distinct=True,
                whereclause=DivisionDBF.uch_vz != u'').alias()

            q = Division.table.insert().from_select(
                ['name', 'vr', 'sni', 'spec'],
                q)
            session.execute(q)

            q = select(
                [DivisionDBF.uch_det.label('name'),
                 DivisionDBF.det_vr.label('vr'),
                 DivisionDBF.sni_det.label('sni'),
                 expression.literal(u'педиатр').label('spec')],
                distinct=True,
                whereclause=DivisionDBF.uch_det != u'').alias()

            q = Division.table.insert().from_select(
                ['name', 'vr', 'sni', 'spec'],
                q)
            session.execute(q)

            q = select(
                [DivisionDBF.uch_ven.label('name'),
                 expression.literal(u'венеролог').label('spec')],
                distinct=True,
                whereclause=DivisionDBF.uch_ven != u'').alias()

            q = Division.table.insert().from_select(
                ['name', 'spec'],
                q)
            session.execute(q)

            q = select(
                [Division.id.label('division_id'),
                 DivisionDBF.name,
                 DivisionDBF.cla_end,
                 DivisionDBF.cla_soc,
                 DivisionDBF.str_end,
                 DivisionDBF.str_soc,
                 DivisionDBF.gs,
                 DivisionDBF.nech_n,
                 DivisionDBF.nech_e,
                 DivisionDBF.chet_n,
                 DivisionDBF.chet_e],
                distinct=True,
                whereclause=or_(
                    and_(func.lower(Division.name) == func.lower(DivisionDBF.uch_vz),
                         func.lower(Division.vr) == func.lower(DivisionDBF.vz_vr)),
                    and_(func.lower(Division.name) == func.lower(DivisionDBF.uch_det),
                         func.lower(Division.vr) == func.lower(DivisionDBF.det_vr)),
                    func.lower(Division.name) == func.lower(DivisionDBF.uch_ven)
                )).with_for_update().alias()

            q = HousesRange.table.insert().from_select(
                ['division_id', 'name', 'cla_end', 'cla_soc', 'str_end', 'str_soc', 'gs', 'nech_n', 'nech_e', 'chet_n',
                 'chet_e'],
                q)
            session.execute(q)

        yield FlushSession(session)
        yield Refresh()


class LinkDivisions(Action):
    verbose_name = _('Link Divisions')

    def model_run(self, model_context):
        from camelot.view.action_steps import (SelectFile,
                                               UpdateProgress,
                                               Refresh,
                                               FlushSession)
        from sqlalchemy import orm
        from camelot.core.orm import Session
        from camelot.core.sql import metadata
        from camelot.core.conf import settings
        from camelot.view.action_steps import MessageBox
        from sqlalchemy.sql import delete
        from sqlalchemy.exc import OperationalError
        from camelot.core.qt import QtGui

        hdtable = Division.metadata.tables['house_division__division_house']

        def clean_stat(session):
            seq = (delete(HouseDivision),
                   )
            for i in seq:
                session.execute(i)

        yield MessageBox(_(u'Do you want to link divisions?'), QtGui.QMessageBox.Question, title=_(u'confirmation'))

        session = model_context.session

        with session.begin():
            clean_stat(session)

            q_a = select(
                [House.id.label('h_id'),
                 House.street_id,
                 House.numb.label('h_numb'),
                 Town.id.label('t_id'),
                 Town.name.label('t_name'),
                 Town.socr.label('t_socr')],
                whereclause=House.town_id == Town.id).with_for_update().alias()

            q_a = outerjoin(
                q_a, Street,
                q_a.c.street_id == Street.id).select().alias()

            q_a = select(
                [q_a.c.h_id,
                 q_a.c.h_numb,
                 q_a.c.id.label('s_id'),
                 q_a.c.name.label('s_name'),
                 q_a.c.socr.label('s_socr'),
                 q_a.c.t_id,
                 q_a.c.t_name,
                 q_a.c.t_socr]).alias()

            q_hr = select(
                [HousesRange.division_id.label('hr_id'),
                 HousesRange.name.label('hr_name'),
                 HousesRange.cla_soc.label('hr_cla_soc'),
                 HousesRange.str_end.label('hr_str_end'),
                 HousesRange.str_soc.label('hr_str_soc'),
                 HousesRange.nech_n.label('hr_nech_n'),
                 HousesRange.nech_e.label('hr_nech_e'),
                 HousesRange.chet_n.label('hr_chet_n'),
                 HousesRange.chet_e.label('hr_chet_e')],
                whereclause=HousesRange.division_id.isnot(None)).with_for_update().alias()

            q_jd = join(
                q_a, q_hr,
                and_(func.lower(q_a.c.t_name) == func.lower(q_hr.c.hr_name),
                     # q_a.c.t_socr == q_hr.c.hr_cla_soc,  # wrong names
                     or_(and_(q_a.c.s_name.is_(None),
                              q_hr.c.hr_str_end.is_(None),
                              q_a.c.s_socr.is_(None),
                              q_hr.c.hr_str_soc.is_(None)),
                         and_(or_(q_a.c.s_name.isnot(None),
                                  q_hr.c.hr_str_end.isnot(None),
                                  q_a.c.s_socr.isnot(None),
                                  q_hr.c.hr_str_soc.isnot(None)),
                              func.lower(q_a.c.s_name) == func.lower(q_hr.c.hr_str_end),
                              func.lower(q_a.c.s_socr) == func.lower(q_hr.c.hr_str_soc))),
                     or_(and_(q_a.c.h_numb % 2 == 1,
                              q_a.c.h_numb >= q_hr.c.hr_nech_n,
                              q_a.c.h_numb <= q_hr.c.hr_nech_e),
                         and_(q_a.c.h_numb % 2 == 0,
                              q_a.c.h_numb >= q_hr.c.hr_chet_n,
                              q_a.c.h_numb <= q_hr.c.hr_chet_e))
                     )
            ).select().alias()

            q = select(
                [q_jd.c.h_id.label('house_id'),
                 q_jd.c.hr_id.label('division_id')],
                distinct=True).alias()

            q = HouseDivision.table.insert().from_select(
                ['house_id', 'division_id'],
                q)
            session.execute(q)

            q = select(
                [HouseDivision.house_id.label('hd_house_id'),
                 HouseDivision.division_id.label('hd_division_id')]).with_for_update().alias()

            q_j = outerjoin(
                q, hdtable,
                and_(q.c.hd_house_id == hdtable.c.house_id,
                     q.c.hd_division_id == hdtable.c.division_id)).select().alias()

            q = select(
                [q_j.c.hd_house_id.label('house_id'),
                 q_j.c.hd_division_id.label('division_id')],
                whereclause=and_(q_j.c.house_id == None,
                                 q_j.c.division_id == None))

            q = hdtable.insert().from_select(
                ['house_id', 'division_id'],
                q)
            session.execute(q)

        yield FlushSession(session)
        yield Refresh()
