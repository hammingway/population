# -*- coding: utf-8 -*-

from datetime import datetime
from camelot.core.utils import ugettext_lazy as _
from camelot.admin.entity_admin import EntityAdmin
from camelot.admin.action.list_filter import ComboBoxFilter, EditorFilter
from camelot.view.art import ColorScheme
# from camelot.view.filters import ComboBoxFilter
from camelot.view.forms import WidgetOnlyForm, Form, TabForm, HBoxForm, Label, GridForm, Stretch, ColumnSpan
from camelot.core.orm import ColumnProperty
from camelot.core.orm.options import using_options
from camelot.admin import table
from sqlalchemy.sql import select, insert, delete, update, join, outerjoin, func, and_, or_, expression, case, desc
from sqlalchemy.orm import aliased
from model import Division, Diagnose, Patient, House, SimplePatient
from patient import Deduplication, ExtractAddress
#from utils import LEN_FAM, LEN_IM, LEN_OTCH, eq_ult, fam_eq, im_eq, ot_eq
from shform import AssignShapeAction, SetupByShapeAction
import requests

PAT_FORMD = ['n5', 'n6', 'n9', 'n10', 'n13', 'n14', 'n15', 'n16', 'n17', 'n27', 'fam', 'im', 'ot', 'drd',
             'pol', 'snils', 'smo_cod', 'cla_end', 'ss_end', 'cla_soc', 'str_end', 'str_soc', 'dom', 'numb',
             'kv', 'naiden', 'skv_uch', 'adres']

PAT_FLDATTRS = {'n5': {'name': u'Полиса серия'}, 'n6': {'name': u'Полиса номер'}, 'fam': {'name': u'Фамилия'},
                'im': {'name': u'Имя'}, 'ot': {'name': u'Отчество'}, 'drd': {'name': u'Дата рождения'},
                'pol': {'name': u'Пол',
                        'choices': lambda o: [(u'м', _('Male')), (u'ж', _('Female')), (None, _('Absent'))]},
                'snils': {'name': u'СНИЛС'}, 'mkb_code': {'name': u'Код МКБ'},
                'mkb_name': {'name': u'Диагноз основной'},
                'cla_end': {'name': u'Н.Пункт'}, 'cla_soc': {'name': u'Тип Н.П.'}, 'ss_end': {'name': u'Сельсовет'},
                'str_end': {'name': u'Улица'}, 'str_soc': {'name': u'Тип улицы'}, 'dom': {'name': u'Дом'},
                'adres': {'name': u'Адрес неточный'}, 'numb': {'name': u'Дом числ.'}, 'kv': {'name': u'Квартира'},
                'unld': {'name': u'Откреплён'}, 'lnkd': {'name': u'Прикреплён'}
                }

PAT_LISTV_ALL = ['id', 'n5', 'n6', 'n9', 'n10', 'n13', 'n14', 'n15', 'n16', 'n17', 'n27', 'fam', 'im', 'ot', 'drd',
                 'pol', 'snils', 'smo_cod', 'ss_end', 'cla_soc', 'cla_end', 'str_soc', 'str_end', 'dom', 'numb',
                 'kv', 'naiden', 'unld', 'lnkd', 'skv_uch', 'adres']

PAT_LISTV_GENERAL = ['n5', 'n6', 'fam', 'im', 'ot', 'drd', 'pol', 'snils', 'ss_end', 'cla_soc', 'cla_end', 'str_soc',
                     'str_end', 'dom', 'unld', 'lnkd', 'adres']


class OrphanedDivision(object):
    # using_options(order_by=['d_spec', 'd_name', 'd_vr'])

    class Admin(EntityAdmin):
        verbose_name = _('Orphaned divisions')
        verbose_name_plural = _('Orphaned divisions')
        list_display = table.Table(['d_name', 'd_vr', 'd_spec'])


class OrphanedPatient(object):
    # using_options(order_by=['fam', 'im', 'ot', 'drd'])

    class Admin(EntityAdmin):
        verbose_name = _('Orphaned patients')
        verbose_name_plural = _('Orphaned patients')
        field_attributes = PAT_FLDATTRS
        list_display = table.Table(
            [table.ColumnGroup(_('General'), PAT_LISTV_GENERAL + ['rnumb']),
             table.ColumnGroup(_('All'), PAT_LISTV_ALL)
             ])
        list_actions = [requests.AppendOrphanedToRequestAction(), ExtractAddress()]
        form_display = PAT_FORMD


class PatientWithDiagn(object):
    class Admin(EntityAdmin):
        verbose_name = _('Patients who has diagnose')
        verbose_name_plural = _('Patients who has diagnose')
        field_attributes = PAT_FLDATTRS
        list_display = table.Table(
            [table.ColumnGroup(_('General'), PAT_LISTV_GENERAL),
             table.ColumnGroup(_('All'), PAT_LISTV_ALL)
             ])
        list_actions = [requests.AppendOrphanedToRequestAction()]
        form_display = PAT_FORMD


class PatientDispanser(object):
    class Admin(EntityAdmin):
        verbose_name = _('Patient to dispanserise')
        verbose_name_plural = _('Patients to dispanserise')
        field_attributes = PAT_FLDATTRS
        field_attributes.update({'kvartal': {'name': u'Квартал'}})
        list_filter = [ComboBoxFilter('smo_cod'), ComboBoxFilter('dmonth'), ComboBoxFilter('kvartal')]
        list_display = table.Table(
            [table.ColumnGroup(_('General'), PAT_LISTV_GENERAL + ['dmonth', 'kvartal']),
             table.ColumnGroup(_('All'), PAT_LISTV_ALL + ['dmonth', 'kvartal'])
             ])
        list_actions = [requests.AppendAllToRequestAction()]
        form_display = PAT_FORMD


class PatientExists(object):
    class Admin(EntityAdmin):
        verbose_name = _('Is our patient to dispanserise')
        verbose_name_plural = _('Is our patients to dispanserise')
        field_attributes = PAT_FLDATTRS
        field_attributes.update({'kvartal': {'name': u'Квартал'}})
        list_filter = [ComboBoxFilter('smo_cod'), ComboBoxFilter('dmonth'), ComboBoxFilter('kvartal')]
        list_display = table.Table(
            [table.ColumnGroup(_('General'), PAT_LISTV_GENERAL + ['dmonth', 'kvartal']),
             table.ColumnGroup(_('All'), PAT_LISTV_ALL + ['dmonth', 'kvartal'])
             ])
        list_actions = [requests.AppendAllToRequestAction()]
        form_display = PAT_FORMD


class PatientAssign(object):
    class Admin(EntityAdmin):
        verbose_name = _('Patients with their assignments')
        verbose_name_plural = _('Patients with their assignments')
        field_attributes = PAT_FLDATTRS
        list_display = table.Table(
            [table.ColumnGroup(_('General'), PAT_LISTV_GENERAL + ['usnils', 'uch']),
             table.ColumnGroup(_('All'), PAT_LISTV_ALL + ['usnils', 'uch'])
             ])
        list_actions = [requests.AppendAllToRequestAction()]
        form_display = PAT_FORMD


class PatientFromList(object):
    # using_options(order_by=['fam', 'im', 'ot', 'drd'])

    class Admin(EntityAdmin):
        verbose_name = _('All patients from list')
        verbose_name_plural = _('All patients from list')
        field_attributes = PAT_FLDATTRS
        list_display = table.Table(
            [table.ColumnGroup(_('General'), PAT_LISTV_GENERAL),
             table.ColumnGroup(_('All'), PAT_LISTV_ALL)
             ])
        list_actions = [requests.AppendAllToRequestAction()]
        form_display = PAT_FORMD


class PatientDuplications(object):
    class Admin(EntityAdmin):
        verbose_name = _('Patients duplications')
        verbose_name_plural = _('Patients duplications')
        field_attributes = PAT_FLDATTRS
        # list_display = ['fam', 'im', 'ot', 'drd']
        list_display = table.Table(
            [table.ColumnGroup(_('General'), PAT_LISTV_GENERAL),
             table.ColumnGroup(_('All'), PAT_LISTV_ALL)
             ])
        list_actions = [Deduplication(), AssignShapeAction(), SetupByShapeAction(), ExtractAddress()]
        form_display = PAT_FORMD

    def __unicode__(self):
        return ('%s %s %s %s %s' % (
            getattr(self, 'fam', None) or '',
            getattr(self, 'im', None) or '',
            getattr(self, 'ot', None) or '',
            getattr(self, 'drd', None) or '',
            getattr(self, 'snils', None) or ''
        )).strip()


class PolisesDuplications(object):
    class Admin(EntityAdmin):
        verbose_name = _('Polises duplications')
        verbose_name_plural = _('Polises duplications')
        field_attributes = PAT_FLDATTRS
        # list_display = ['fam', 'im', 'ot', 'drd']
        list_display = table.Table(
            [table.ColumnGroup(_('General'), PAT_LISTV_GENERAL),
             table.ColumnGroup(_('All'), PAT_LISTV_ALL)
             ])
        list_actions = [Deduplication(), AssignShapeAction(), SetupByShapeAction(), ExtractAddress()]
        form_display = PAT_FORMD

    def __unicode__(self):
        return ('%s %s %s %s %s' % (
            getattr(self, 'fam', None) or '',
            getattr(self, 'im', None) or '',
            getattr(self, 'ot', None) or '',
            getattr(self, 'drd', None) or '',
            getattr(self, 'snils', None) or ''
        )).strip()


class PatientProf(object):
    class Admin(EntityAdmin):
        verbose_name = _('Is our patient to prof')
        verbose_name_plural = _('Is our patients to prof')
        field_attributes = PAT_FLDATTRS
        field_attributes.update({'kvartal': {'name': u'Квартал'}})
        list_filter = [ComboBoxFilter('smo_cod'), ComboBoxFilter('dmonth'), ComboBoxFilter('kvartal')]
        list_display = table.Table(
            [table.ColumnGroup(_('General'), PAT_LISTV_GENERAL + ['dmonth', 'kvartal']),
             table.ColumnGroup(_('All'), PAT_LISTV_ALL + ['dmonth', 'kvartal', 'house_codes', 'house_index'])
             ])
        list_actions = [requests.AppendAllToRequestAction()]
        form_display = PAT_FORMD


class LinkedPatient(object):
    class Admin(EntityAdmin):
        verbose_name = _('Linked patients')
        verbose_name_plural = _('Linked patients')
        field_attributes = PAT_FLDATTRS
        list_filter = [EditorFilter('n6'), EditorFilter('fam'), EditorFilter('im'), EditorFilter('ot')]
        list_display = table.Table(
            [table.ColumnGroup(_('General'), PAT_LISTV_GENERAL),
             table.ColumnGroup(_('All'), PAT_LISTV_ALL)
             ])
        list_actions = [requests.AppendAllToRequestAction()]
        form_display = PAT_FORMD


# class AllSimple(object):
#     # using_options(order_by=['fam', 'im', 'ot', 'drd'])
#
#     class Admin(EntityAdmin):
#         verbose_name = _('All list')
#         verbose_name_plural = _('All list')
#         field_attributes = PAT_FLDATTRS
#         list_display = table.Table(
#             [table.ColumnGroup(_('General'), PAT_LISTV_GENERAL),
#              table.ColumnGroup(_('All'), PAT_LISTV_ALL)
#              ])
#         list_actions = [requests.AppendAllToRequestAction()]
#         form_display = PAT_FORMD


def setup_views():
    from sqlalchemy.sql import select, outerjoin, join, func, literal, and_, or_
    from sqlalchemy.orm import mapper, Session
    from utils import LEN_FAM, LEN_IM, LEN_OTCH, fam_eq, im_eq, ot_eq

    hdtable = Division.metadata.tables['house_division__division_house']

    q = select(
        [Division.id.label('d_id'),
         Division.name.label('d_name'),
         Division.vr.label('d_vr'),
         Division.spec.label('d_spec')],
        whereclause=Division.id.notin_(select([hdtable.c.division_id]).alias())) \
        .order_by(Division.spec, Division.name, Division.vr).alias()

    mapper(OrphanedDivision, q)

    pdtable = Patient.metadata.tables['patient_division__division_patient']

    q = select(
        [Patient,
         func.row_number().over(order_by=[Patient.fam, Patient.im, Patient.ot]).label('rnumb')
         ],
        whereclause=and_(pdtable.c.patient_id == None, Patient.house_id == None)) \
        .select_from(Patient.table.outerjoin(pdtable)) \
        .order_by(Patient.fam, Patient.im, Patient.ot, Patient.drd).alias()

    q = select(
        [q],
        # whereclause=and_(q.c.rnumb > 200, q.c.rnumb < 300)
    ).alias()

    mapper(OrphanedPatient, q)

    q = select(
        [Patient,
         Diagnose.kod_mkb.label('mkb_code'),
         Diagnose.name.label('mkb_name')],
        whereclause=Patient.mkb_id == Diagnose.id) \
        .order_by(Patient.fam, Patient.im, Patient.ot, Patient.drd).alias()

    mapper(PatientWithDiagn, q)

    # (1917, 1920, 1923, 1926, 1929, 1932, 1935, 1938, 1941, 1944, 1947, 1950, 1953, 1956, 1959, 1962,
    # 1965, 1968, 1971, 1974, 1977, 1980, 1983, 1986, 1989, 1992, 1995)

    year_in_set = func.extract('year', Patient.drd).in_(
        (1918, 1921, 1924, 1927, 1930, 1933, 1936, 1939, 1942, 1945, 1948, 1951, 1954, 1957, 1960,
         1963, 1966, 1969, 1972, 1975, 1978, 1981, 1984, 1987, 1990, 1993, 1996))
    year_notin_set = func.extract('year', Patient.drd).notin_(
        (1918, 1921, 1924, 1927, 1930, 1933, 1936, 1939, 1942, 1945, 1948, 1951, 1954, 1957, 1960,
         1963, 1966, 1969, 1972, 1975, 1978, 1981, 1984, 1987, 1990, 1993, 1996))
    q = select(
        [Patient.id],
        whereclause=and_(
            year_in_set,
            fam_eq(Patient, SimplePatient),
            im_eq(Patient, SimplePatient),
            ot_eq(Patient, SimplePatient),
            Patient.drd == SimplePatient.drd
        )).alias()
    q = select(
        [Patient,
         func.extract('month', Patient.drd).label('dmonth'),
         case(
             [(and_(func.extract('month', Patient.drd) >= 1,
                    func.extract('month', Patient.drd) < 4),
               expression.literal(1)),
              (and_(func.extract('month', Patient.drd) >= 4,
                    func.extract('month', Patient.drd) < 7),
               expression.literal(2)),
              (and_(func.extract('month', Patient.drd) >= 7,
                    func.extract('month', Patient.drd) < 10),
               expression.literal(3))
              ],
             else_=expression.literal(4)).label('kvartal')
         ],
        whereclause=and_(
            year_in_set,
            Patient.id.notin_(q)
        )) \
        .order_by(Patient.fam, Patient.im, Patient.ot, Patient.drd).alias()

    mapper(PatientDispanser, q)

    q = select(
        [Patient,
         func.extract('month', Patient.drd).label('dmonth'),
         case(
             [(and_(func.extract('month', Patient.drd) >= 1,
                    func.extract('month', Patient.drd) < 4),
               expression.literal(1)),
              (and_(func.extract('month', Patient.drd) >= 4,
                    func.extract('month', Patient.drd) < 7),
               expression.literal(2)),
              (and_(func.extract('month', Patient.drd) >= 7,
                    func.extract('month', Patient.drd) < 10),
               expression.literal(3))
              ],
             else_=expression.literal(4)).label('kvartal')
         ],
        whereclause=year_in_set) \
        .select_from(Patient.table.join(SimplePatient, and_(
        # Patient.fam == SimplePatient.fam,
        # Patient.im == SimplePatient.im,
        # eq_ult(Patient.ot, SimplePatient.ot),
        fam_eq(Patient, SimplePatient),
        im_eq(Patient, SimplePatient),
        ot_eq(Patient, SimplePatient),
        Patient.drd == SimplePatient.drd
    ))).alias()  # \
    # .order_by(Patient.fam, Patient.im, Patient.ot, Patient.drd).alias()

    mapper(PatientExists, q)

    q = select(
        [Patient,
         Division.id.label('division_id'),
         Division.sni.label('usnils'),
         Division.name.label('uch')],
        whereclause=Division.spec == u'взрослые') \
        .select_from(Patient.table
                     .join(pdtable, pdtable.c.patient_id == Patient.id)
                     .join(Division, pdtable.c.division_id == Division.id)) \
        .order_by(Patient.fam, Patient.im, Patient.ot, Patient.drd).alias()

    mapper(PatientAssign, q)

    q = select(
        [Patient,
         # Division.id.label('division_id'),
         # Division.sni.label('usnils'),
         # Division.name.label('uch')
         ],
        # whereclause=and_(
        #     Division.spec.notin_((u'венеролог'))
        # )
    ) \
        .select_from(Patient.table.join(SimplePatient, and_(
        # Patient.fam == SimplePatient.fam,
        # Patient.im == SimplePatient.im,
        # eq_ult(Patient.ot, SimplePatient.ot),
        fam_eq(Patient, SimplePatient),
        im_eq(Patient, SimplePatient),
        ot_eq(Patient, SimplePatient),
        Patient.drd == SimplePatient.drd
    ))).alias()  # .outerjoin(pdtable).outerjoin(Division)

    mapper(PatientFromList, q)

    q = select(
        [Patient.id,
         func.substring(Patient.fam, 1, LEN_FAM).label('fam'),
         func.substring(Patient.im, 1, LEN_IM).label('im'),
         func.substring(Patient.ot, 1, LEN_OTCH).label('ot'),
         Patient.drd]
    ).alias()

    q = select(
        [q.c.fam,
         q.c.im,
         q.c.ot,
         q.c.drd]
    ) \
        .group_by(q.c.fam, q.c.im, q.c.ot, q.c.drd) \
        .having(func.count(q.c.id) > 1).alias()

    q = select(
        [Patient]
    ).select_from(
        Patient.table.join(
            q,
            and_(fam_eq(q.c, Patient),
                 im_eq(q.c, Patient),
                 ot_eq(q.c, Patient),
                 q.c.drd == Patient.drd)
        )
    ).alias()

    # q = select(
    #     [Patient.fam, Patient.im, Patient.ot, Patient.drd, func.count(Patient.id).label('count')]
    # ) \
    #     .group_by(Patient.fam, Patient.im, Patient.ot, Patient.drd) \
    #     .having(func.count(Patient.id) > 1).alias()

    mapper(PatientDuplications, q,
           order_by=[
               func.substring(q.c.fam, 1, LEN_FAM),
               func.substring(q.c.im, 1, LEN_IM),
               func.substring(q.c.ot, 1, LEN_OTCH),
               q.c.drd
           ])

    q = select(
        [Patient.n6]
    ) \
        .group_by(Patient.n6) \
        .having(func.count(Patient.id) > 1).alias()

    q = select(
        [Patient]
    ).select_from(
        Patient.table.join(
            q,
            q.c.n6 == Patient.n6
        )
    ).alias()

    # q = select(
    #     [Patient.fam, Patient.im, Patient.ot, Patient.drd, func.count(Patient.id).label('count')]
    # ) \
    #     .group_by(Patient.fam, Patient.im, Patient.ot, Patient.drd) \
    #     .having(func.count(Patient.id) > 1).alias()

    mapper(PolisesDuplications, q, order_by=[q.c.n6, q.c.fam, q.c.im, q.c.ot, q.c.drd])

    today = datetime.today()
    dbound = today.replace(year=today.year - 60)

    q = select(
        [Patient.id],
        whereclause=and_(
            fam_eq(Patient, SimplePatient),
            im_eq(Patient, SimplePatient),
            ot_eq(Patient, SimplePatient),
            Patient.drd == SimplePatient.drd
        )).alias()
    q = select(
        [Patient,
         House.codes.label('house_codes'),
         House.index.label('house_index'),
         func.extract('month', Patient.drd).label('dmonth'),
         case(
             [(and_(func.extract('month', Patient.drd) >= 1,
                    func.extract('month', Patient.drd) < 4),
               expression.literal(1)),
              (and_(func.extract('month', Patient.drd) >= 4,
                    func.extract('month', Patient.drd) < 7),
               expression.literal(2)),
              (and_(func.extract('month', Patient.drd) >= 7,
                    func.extract('month', Patient.drd) < 10),
               expression.literal(3))
              ],
             else_=expression.literal(4)).label('kvartal')
         ],
        whereclause=and_(
            year_notin_set,
            Patient.id.notin_(q),
            Patient.drd <= dbound
        )).select_from(Patient.table.outerjoin(House)) \
        .order_by(Patient.fam, Patient.im, Patient.ot, Patient.drd).alias()

    mapper(PatientProf, q)

    q = select([Patient],
               whereclause=or_(Patient.lnkd > Patient.unld,
                               and_(Patient.lnkd.isnot(None), Patient.unld.is_(None)))
               ).alias()

    mapper(LinkedPatient, q, always_refresh=True)

    # q = select([SimplePatient]).alias()
    #
    # mapper(AllSimple, q)
