# -*- coding: utf-8 -*-

import re
import ahocorasick

from sqlalchemy import func, and_, or_

from model import Town, Street, Patient, PatientDBF


class ElementType:
    TOWN = 1
    STREET = 2
    TOWN_S = 4
    STREET_S = 8


def _norm_name(name):
    return name.strip().upper()


class Addresses(object):
    numbre = re.compile(r'\d+')

    def __init__(self):
        self.ac = ahocorasick.Automaton()
        self.acinitialized = False
        self.have_towns = False
        self.have_streets = False
        self.patients = None

    def towns_import(self, session, force=False):
        if self.have_towns and not force:
            return
        data = session.query(Town.name, Town.socr).all()
        for n, s in data:
            n, s = _norm_name(n), _norm_name(s)
            un = ' %s ' % n.encode('utf-8')
            item = self.ac.get(un, None)
            if item is None:
                # Тип элемента, имя, городоов сокр, улицы города, улицы сокр
                self.ac.add_word(un, [ElementType.TOWN, n, {s}, set(), set()])
            elif item[0] & (ElementType.TOWN + ElementType.STREET):
                item[0] |= ElementType.TOWN
                item[2].add(s)
            else:
                raise Exception('Elements types conflict for "%s"' % n.strip())
            for shape in (' %s ', '(%s)'):
                us = shape % s.encode('utf-8')
                item = self.ac.get(us, None)
                if item is None:
                    self.ac.add_word(us, [ElementType.TOWN_S, s])
                elif item[0] & (ElementType.TOWN_S + ElementType.STREET_S):
                    item[0] |= ElementType.TOWN_S
                else:
                    raise Exception('Elements types conflict for "%s"' % s.strip())

        self.have_towns = True
        self.acinitialized = False

    def streets_import(self, session, force=False):
        if self.have_streets and not force:
            return
        data = session.query(Street.name, Street.socr, Town.name.label('town')).join(Town).all()
        for n, s, t in data:
            n, s, t = _norm_name(n), _norm_name(s), _norm_name(t)
            un = ' %s ' % n.encode('utf-8')
            item = self.ac.get(un, None)
            if item is None:
                # Тип элемента, имя, городов сокр, улицы города, улицы сокр
                self.ac.add_word(un, [ElementType.STREET, n, set(), {t}, {s}])
            elif item[0] & (ElementType.TOWN + ElementType.STREET):
                item[0] |= ElementType.STREET
                item[3].add(t)
                item[4].add(s)
            else:
                raise Exception('Elements types conflict for "%s"' % n.strip())
            for shape in (' %s ', '(%s)'):
                us = shape % s.encode('utf-8')
                item = self.ac.get(us, None)
                if item is None:
                    self.ac.add_word(us, [ElementType.STREET_S, s])
                elif item[0] & (ElementType.TOWN_S + ElementType.STREET_S):
                    item[0] |= ElementType.STREET_S
                else:
                    raise Exception('Elements types conflict for "%s"' % s.strip())
        self.have_streets = True
        self.acinitialized = False

    def patients_load(self, session):
        self.patients = \
            session.query(Patient.id, func.upper(Patient.adres).label('adres')) \
                .filter(
                and_(
                    Patient.adres.isnot(None),
                    Patient.cla_end.is_(None),
                    Patient.cla_soc.is_(None),
                    Patient.str_end.is_(None),
                    Patient.str_soc.is_(None),
                    or_(Patient.numb.is_(None), Patient.numb == 0),
                    Patient.kv.is_(None)
                )).order_by(Patient.adres).all()

    def addresses_fill(self, session):
        from camelot.view.action_steps import FlushSession
        for i, (id_, adres) in enumerate(self.patients):
            a = self.addr_normalize(adres)
            if a:
                p = PatientDBF()
                p.id = id_
                p.cla_end = a['cla_end']
                p.cla_soc = a['cla_soc']
                p.str_end = a['str_end']
                p.str_soc = a['str_soc']
                p.dom = a['numb']
                p.kv = a['kv']
                p.numb = int(a['numb'])
            if i % 200 == 0:
                yield FlushSession(session)
        yield FlushSession(session)

    def addr_normalize(self, addr):
        from operator import itemgetter
        from itertools import product

        town_found = None
        town_socr = None
        street_found = None
        street_socr = None
        bld_found = None
        ap_found = None

        if not self.acinitialized:
            self.ac.make_automaton()
            self.acinitialized = True

        addr = ' %s ' % _norm_name(addr.replace(',', ' ')).encode('utf-8')
        towns = []
        streets = []
        townset = set()
        streetset = set()
        towns_s = {}
        streets_s = {}
        for end_i, val in self.ac.iter(addr):
            # (itype, name, town_s, street_t, street_s)
            if val[0] & ElementType.TOWN:
                towns.append((end_i, val[1], val[2]))
                townset.add(val[1])
            if val[0] & ElementType.STREET:
                streets.append((end_i, val[1], val[3], val[4]))
                streetset.add(val[1])
            if val[0] & ElementType.TOWN_S:
                towns_s[val[1]] = end_i
            if val[0] & ElementType.STREET_S:
                streets_s[val[1]] = end_i

        def intersects(ai, a, bi, b):
            # Учитывая граничные пробелы
            sta = ai - len(a)
            stb = bi - len(b)
            return not (sta > bi - 1 or stb > ai - 1)

        def number_get(*indexes):
            matches = self.numbre.findall(addr[max(indexes):])
            bld = ap = None
            if matches:
                bld = matches[0]
            if len(matches) > 1:
                ap = matches[1]
            return bld, ap

        def process_pair(street_i, street_n, street_t, street_s, town_i, town_n, town_s):
            if (town_n not in street_t
                    or intersects(town_i, town_n, street_i, street_n)
                    or not towns_s and len(town_s) > 1
                    or not streets_s and len(street_s) > 1
                    or town_s and towns_s and not len(town_s.intersection(towns_s)) == 1
                    or street_s and streets_s and not len(street_s.intersection(streets_s)) == 1
            ):
                return
            towns_i = tuple(town_s.intersection(towns_s))[0] if town_s and towns_s else None
            streets_i = tuple(street_s.intersection(streets_s))[0] if street_s and streets_s else (
                tuple(street_s)[0] if len(street_s) == 1 else None)
            bld, ap = number_get(street_i, town_i)
            return town_n, towns_i, street_n, streets_i, bld, ap

        def process_town(town_i, town_n, town_s):
            if (not towns_s and len(town_s) > 1
                    or town_s and towns_s and not len(town_s.intersection(towns_s)) == 1
            ):
                return
            towns_i = tuple(town_s.intersection(towns_s))[0] if town_s and towns_s else None
            bld, ap = number_get(town_i)
            return town_n, towns_i, bld, ap

        if len(streets) == 1 and len(towns) == 1:
            result = process_pair(*streets[0] + towns[0])
            if result is not None:
                town_found, town_socr, street_found, street_socr, bld_found, ap_found = result
        elif len(streets) >= 1 and len(towns) >= 1:
            for street, town in product(sorted(streets, key=itemgetter(0), reverse=True),
                                        sorted(towns, key=itemgetter(0), reverse=True)):
                result = process_pair(*street + town)
                if result is not None:
                    town_found, town_socr, street_found, street_socr, bld_found, ap_found = result
                    break
        elif len(streets) == 0 and len(towns) >= 1:
            for town in sorted(towns, key=itemgetter(0), reverse=True):
                result = process_town(*town)
                if result is not None:
                    town_found, town_socr, bld_found, ap_found = result
                    break

        if town_found is not None and bld_found is not None:
            return {'cla_end': town_found,
                    'cla_soc': town_socr,
                    'str_end': street_found,
                    'str_soc': street_socr,
                    'numb': bld_found,
                    'kv': ap_found
                    }
