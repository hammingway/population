import logging
import sys

from camelot.core.utils import ugettext as _
from camelot.admin.action.application import Application
from camelot.core.qt import *
from addresses import Addresses

logger = logging.getLogger('main')

the_application = None


class MyApplication(Application):
    def __init__(self, application_admin=None):
        super(MyApplication, self).__init__(application_admin)
        self.patient_shape = None
        self.addresses = Addresses()

    def model_run(self, model_context):
        from camelot.core.conf import settings
        from camelot.core.utils import load_translations
        from camelot.view import action_steps
        from camelot.core.sql import metadata
        from camelot.core.profile import ProfileStore
        from camelot.admin.action.application_action import SelectProfile

        yield action_steps.UpdateProgress(1, 5, _('Install translator'))
        yield action_steps.InstallTranslator(model_context.admin)

        yield action_steps.UpdateProgress(2, 5, _('Setup database'))
        profile_store = ProfileStore()
        profile = yield SelectProfile(profile_store)
        metadata.bind = profile.create_engine()
        settings.setup_model()

        yield action_steps.UpdateProgress(3, 5, _('Load translations'))
        load_translations()
        yield action_steps.UpdateProgress(4, 5, _('Create main window'))
        yield action_steps.MainWindow(self.application_admin)
        home_actions = self.application_admin.get_actions()
        yield action_steps.ActionView(_('Home'), home_actions)
