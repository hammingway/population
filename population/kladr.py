# -*- coding: utf-8 -*-

from camelot.admin.action import Action
from camelot.core.utils import ugettext_lazy as _
from camelot.core.orm import setup_all
from sqlalchemy.ext import compiler
from sqlalchemy import Integer
from sqlalchemy.sql import select, update, delete, join, func, and_, expression
from model import Kladr, KladrStreet, KladrDoma, KladrAltnames, Town, Street, House
#from utils import iter_dbf
import os


def load_temporaries(session, dir_name='.', full=True):
    from camelot.view.action_steps import FlushSession
    from utils import iter_dbf

    # def prepare_name(name):
    #     p = name.split('(')
    #     p = [i.strip().replace(')', '').upper() for i in p]
    #     if len(p) < 2:
    #         p.append(None)
    #     return p[0:2]

    for i, r in iter_dbf(os.path.join(dir_name, 'KLADR.DBF')):
        if r.code.startswith(u'52000005'):
            k = Kladr()
            k.name = r.name
            k.socr = r.socr.lower()
            k.code = r.code
            k.index = r.index
            k.gninmb = r.gninmb
            k.uno = r.uno
            k.ocatd = r.ocatd
            k.status = r.status
            if i % 200 == 0:
                yield FlushSession(session)

    for i, r in iter_dbf(os.path.join(dir_name, 'STREET.DBF')):
        if r.code.startswith(u'52000005'):
            k = KladrStreet()
            k.name = r.name
            k.socr = r.socr.lower()
            k.code = r.code
            k.index = r.index
            k.gninmb = r.gninmb
            k.uno = r.uno
            k.ocatd = r.ocatd
            if i % 200 == 0:
                yield FlushSession(session)

    def mk_pint(val):
        ival = -1
        try:
            ival = int(val)
        except ValueError:
            pass
        return ival

    for i, r in iter_dbf(os.path.join(dir_name, 'DOMA.DBF')):
        if r.code.startswith(u'52000005'):
            for d in r.name.split(','):
                k = KladrDoma()
                k.name = d
                k.numb = mk_pint(d.replace(d.lstrip('1234567890'), ''))
                k.korp = r.korp
                k.socr = r.socr.lower()
                k.code = r.code
                k.index = r.index
                k.gninmb = r.gninmb
                k.uno = r.uno
                k.ocatd = r.ocatd
            if i % 200 == 0:
                yield FlushSession(session)

    if full:
        for i, r in iter_dbf(os.path.join(dir_name, 'ALTNAMES.DBF')):
            if r.oldcode.startswith(u'52000005') or r.newcode.startswith(u'52000005'):
                k = KladrAltnames()
                k.oldcode = r.oldcode
                k.newcode = r.newcode
                k.level = r.level
                if i % 200 == 0:
                    yield FlushSession(session)

    yield FlushSession(session)


class ImportKladr(Action):
    verbose_name = _('Import Kladr')

    def model_run(self, model_context):
        from camelot.view.action_steps import (SelectFile,
                                               SelectDirectory,
                                               UpdateProgress,
                                               Refresh,
                                               FlushSession)
        from sqlalchemy import orm
        from camelot.core.orm import Session
        from camelot.core.sql import metadata
        from camelot.core.conf import settings
        from camelot.view.action_steps import MessageBox
        from sqlalchemy.sql import delete
        from sqlalchemy.exc import OperationalError
        import PyQt4.QtGui as QtGui

        select_archives = SelectDirectory()
        file_names = yield select_archives
        if not file_names:
            return
        dir_name = file_names

        def clean_stat(session):
            seq = (
                delete(House.metadata.tables['house_division__division_house']),
                delete(House.table),
                delete(Street.table),
                delete(Town.table))
            for i in seq:
                session.execute(i)

        def clean_temp(session):
            seq = (
                delete(Kladr.table),
                delete(KladrStreet.table),
                delete(KladrDoma.table),
                delete(KladrAltnames.table))
            for i in seq:
                session.execute(i)

        # session = Session()
        session = model_context.session

        clean_temp(session)

        for i in load_temporaries(session, dir_name, full=False):
            yield i

        with session.begin():
            clean_stat(session)

            q = select(
                [Kladr.name,
                 Kladr.socr,
                 func.substring(Kladr.code, 1, 2).label('codes'),
                 func.substring(Kladr.code, 3, 3).label('coder'),
                 func.substring(Kladr.code, 6, 3).label('codeg'),
                 func.substring(Kladr.code, 9, 3).label('codep'),
                 Kladr.index,
                 Kladr.gninmb,
                 Kladr.uno,
                 Kladr.ocatd],
                whereclause=func.substring(Kladr.code, 12, 2) == u'00').alias()

            q = Town.table.insert().from_select(
                ['name', 'socr', 'codes', 'coder', 'codeg', 'codep', 'index', 'gninmb', 'uno', 'ocatd'],
                q)
            session.execute(q)

            q = select(
                [Town.id.label('town_id'),
                 KladrStreet.name,
                 KladrStreet.socr,
                 Town.codes,
                 Town.coder,
                 Town.codeg,
                 Town.codep,
                 func.substring(KladrStreet.code, 12, 4).label('codeu'),
                 KladrStreet.index,
                 KladrStreet.gninmb,
                 KladrStreet.uno,
                 KladrStreet.ocatd]).select_from(
                KladrStreet.table.join(
                    Town,
                    and_(func.substring(KladrStreet.code, 1, 2) == Town.codes,
                         func.substring(KladrStreet.code, 3, 3) == Town.coder,
                         func.substring(KladrStreet.code, 6, 3) == Town.codeg,
                         func.substring(KladrStreet.code, 9, 3) == Town.codep,
                         func.substring(KladrStreet.code, 16, 2) == u'00'))).alias()

            q = Street.table.insert().from_select(
                ['town_id', 'name', 'socr', 'codes', 'coder', 'codeg', 'codep', 'codeu', 'index', 'gninmb', 'uno',
                 'ocatd'],
                q)
            session.execute(q)

            q = select(
                [Town.id.label('town_id'),
                 Street.id.label('street_id'),
                 KladrDoma.name,
                 KladrDoma.numb,
                 KladrDoma.socr,
                 Town.codes,
                 Town.coder,
                 Town.codeg,
                 Town.codep,
                 Street.codeu,
                 func.substring(KladrDoma.code, 16, 4).label('coded'),
                 KladrDoma.index,
                 KladrDoma.gninmb,
                 KladrDoma.uno,
                 KladrDoma.ocatd]).select_from(
                KladrDoma.table
                    .join(Town,
                          and_(func.substring(KladrDoma.code, 1, 2) == Town.codes,
                               func.substring(KladrDoma.code, 3, 3) == Town.coder,
                               func.substring(KladrDoma.code, 6, 3) == Town.codeg,
                               func.substring(KladrDoma.code, 9, 3) == Town.codep))
                    .join(Street,
                          and_(Town.codes == Street.codes,
                               Town.coder == Street.coder,
                               Town.codeg == Street.codeg,
                               Town.codep == Street.codep,
                               func.substring(KladrDoma.code, 12, 4) == Street.codeu))).alias()

            q = House.table.insert().from_select(
                ['town_id', 'street_id', 'name', 'numb', 'socr', 'codes', 'coder', 'codeg', 'codep', 'codeu', 'coded',
                 'index', 'gninmb', 'uno', 'ocatd'],
                q)
            session.execute(q)

        clean_temp(session)
        yield FlushSession(session)

        yield Refresh()


class UpdateKladr(Action):
    verbose_name = _('Update Kladr')

    def model_run(self, model_context):
        from camelot.view.action_steps import (SelectFile,
                                               SelectDirectory,
                                               UpdateProgress,
                                               Refresh,
                                               FlushSession)
        from sqlalchemy import orm
        from camelot.core.orm import Session
        from camelot.core.sql import metadata
        from camelot.core.conf import settings
        from camelot.view.action_steps import MessageBox
        from sqlalchemy.sql import delete
        from sqlalchemy.exc import OperationalError
        from camelot.core.qt import QtGui

        select_archives = SelectDirectory()
        file_names = yield select_archives
        if not file_names:
            return
        dir_name = file_names

        def clean_temp(session):
            seq = (delete(Kladr.table),
                   delete(KladrStreet.table),
                   delete(KladrDoma.table),
                   delete(KladrAltnames.table))
            for i in seq:
                session.execute(i)

        def reassign(session):
            baselevel = 5
            lowest = 3
            facets = ((1, 2, 'codes'), (3, 3, 'coder'), (6, 3, 'codeg'), (9, 3, 'codep'), (12, 4, 'codeu'))
            tables = (Town, Town, Town, Town, Street, House)
            #  Ищем смену подчинения с верхних уровней к нижним
            for level in xrange(baselevel, 0, -1):
                basetbl = tables[level - 1]
                zerobnd = baselevel if level == baselevel else baselevel - 1
                bcond = [getattr(basetbl, fld) == func.substring(KladrAltnames.oldcode, st, ln)
                         for st, ln, fld in facets[:level]] + \
                        [func.substring(KladrAltnames.oldcode, st, ln) == u'0' * ln
                         for st, ln, fld in facets[level:zerobnd]]
                match = session.query(basetbl).filter(and_(*bcond)).first()
                if not match:
                    continue
                # Переподчиняем все от текущего уровня вверх к базовому
                for updlvl in xrange(max(level - 1, lowest), baselevel):
                    updtbl = tables[updlvl]
                    ucond = and_(
                        *([getattr(updtbl, fld) == func.substring(KladrAltnames.oldcode, st, ln)
                           for st, ln, fld in facets[:level]] +
                          [KladrAltnames.level.cast(Integer) == level]))
                    # + \
                    # [func.substring(KladrAltnames.oldcode, st, ln) == u'0' * ln
                    #  for st, ln, fld in facets[level:zerobnd]]
                    q = update(
                        updtbl,
                        whereclause=ucond,
                        values={
                            fld: func.substring(KladrAltnames.newcode, st, ln)
                            for st, ln, fld in facets[:level]})
                    session.execute(q)
                    if updlvl > level - 1:
                        q = select(
                            [updtbl.id]).select_from(
                            updtbl.table.join(
                                KladrAltnames,
                                ucond
                            )).with_for_update().alias()
                        q = delete(
                            updtbl,
                            whereclause=updtbl.id.in_(q))
                        session.execute(q)

        # session = Session()
        session = model_context.session

        clean_temp(session)

        for i in load_temporaries(session, dir_name):
            yield i

        with session.begin():
            reassign(session)

            # Towns
            q_o = select(
                [func.substring(Kladr.code, 1, 2).label('codes_o'),
                 func.substring(Kladr.code, 3, 3).label('coder_o'),
                 func.substring(Kladr.code, 6, 3).label('codeg_o'),
                 func.substring(Kladr.code, 9, 3).label('codep_o')],
                whereclause=func.substring(Kladr.code, 12, 2).cast(Integer).between(0, 50)).alias()

            q_n = select(
                [Kladr.name,
                 Kladr.socr,
                 func.substring(Kladr.code, 1, 2).label('codes'),
                 func.substring(Kladr.code, 3, 3).label('coder'),
                 func.substring(Kladr.code, 6, 3).label('codeg'),
                 func.substring(Kladr.code, 9, 3).label('codep'),
                 Kladr.index,
                 Kladr.gninmb,
                 Kladr.uno,
                 Kladr.ocatd],
                whereclause=func.substring(Kladr.code, 12, 2) == u'00').alias()

            q_j = join(
                q_n, q_o,
                and_(
                    q_n.c.codes == q_o.c.codes_o,
                    q_n.c.coder == q_o.c.coder_o,
                    q_n.c.codeg == q_o.c.codeg_o,
                    q_n.c.codep == q_o.c.codep_o
                )).select().alias()

            q = update(
                Town,
                whereclause=and_(
                    Town.codes == q_j.c.codes,
                    Town.coder == q_j.c.coder,
                    Town.codeg == q_j.c.codeg,
                    Town.codep == q_j.c.codep),
                values={
                    'name': q_j.c.name, 'socr': q_j.c.socr, 'index': q_j.c.index, 'gninmb': q_j.c.gninmb,
                    'uno': q_j.c.uno, 'ocatd': q_j.c.ocatd
                })
            session.execute(q)

            # Streets
            q_o = select(
                [func.substring(KladrStreet.code, 1, 2).label('codes_o'),
                 func.substring(KladrStreet.code, 3, 3).label('coder_o'),
                 func.substring(KladrStreet.code, 6, 3).label('codeg_o'),
                 func.substring(KladrStreet.code, 9, 3).label('codep_o'),
                 func.substring(KladrStreet.code, 12, 4).label('codeu_o')],
                whereclause=func.substring(KladrStreet.code, 12, 2).cast(Integer).between(0, 50)).alias()

            q_n = select(
                [KladrStreet.name,
                 KladrStreet.socr,
                 func.substring(KladrStreet.code, 1, 2).label('codes'),
                 func.substring(KladrStreet.code, 3, 3).label('coder'),
                 func.substring(KladrStreet.code, 6, 3).label('codeg'),
                 func.substring(KladrStreet.code, 9, 3).label('codep'),
                 func.substring(KladrStreet.code, 12, 4).label('codeu'),
                 KladrStreet.index,
                 KladrStreet.gninmb,
                 KladrStreet.uno,
                 KladrStreet.ocatd],
                whereclause=func.substring(Kladr.code, 12, 2) == u'00').alias()

            q_j = join(
                q_n, q_o,
                and_(
                    q_n.c.codes == q_o.c.codes_o,
                    q_n.c.coder == q_o.c.coder_o,
                    q_n.c.codeg == q_o.c.codeg_o,
                    q_n.c.codep == q_o.c.codep_o,
                    q_n.c.codeu == q_o.c.codeu_o
                )).select().alias()

            q = update(
                Street,
                whereclause=and_(
                    Street.codes == q_j.c.codes,
                    Street.coder == q_j.c.coder,
                    Street.codeg == q_j.c.codeg,
                    Street.codep == q_j.c.codep,
                    Street.codeu == q_j.c.codeu
                ),
                values={
                    'name': q_j.c.name, 'socr': q_j.c.socr, 'index': q_j.c.index, 'gninmb': q_j.c.gninmb,
                    'uno': q_j.c.uno, 'ocatd': q_j.c.ocatd
                })
            session.execute(q)

            q = select(
                [Kladr.name,
                 Kladr.socr,
                 func.substring(Kladr.code, 1, 2).label('codes'),
                 func.substring(Kladr.code, 3, 3).label('coder'),
                 func.substring(Kladr.code, 6, 3).label('codeg'),
                 func.substring(Kladr.code, 9, 3).label('codep'),
                 Kladr.index,
                 Kladr.gninmb,
                 Kladr.uno,
                 Kladr.ocatd],
                whereclause=and_(
                    Town.id.is_(None),
                    func.substring(Kladr.code, 12, 2) == u'00')).select_from(
                Kladr.table.outerjoin(
                    Town,
                    and_(func.substring(Kladr.code, 1, 2) == Town.codes,
                         func.substring(Kladr.code, 3, 3) == Town.coder,
                         func.substring(Kladr.code, 6, 3) == Town.codeg,
                         func.substring(Kladr.code, 9, 3) == Town.codep))).alias()

            q = Town.table.insert().from_select(
                ['name', 'socr', 'codes', 'coder', 'codeg', 'codep', 'index', 'gninmb', 'uno', 'ocatd'],
                q)
            session.execute(q)

            q = select(
                [KladrStreet.name,
                 KladrStreet.socr,
                 func.substring(KladrStreet.code, 1, 2).label('codes'),
                 func.substring(KladrStreet.code, 3, 3).label('coder'),
                 func.substring(KladrStreet.code, 6, 3).label('codeg'),
                 func.substring(KladrStreet.code, 9, 3).label('codep'),
                 func.substring(KladrStreet.code, 12, 4).label('codeu'),
                 KladrStreet.index,
                 KladrStreet.gninmb,
                 KladrStreet.uno,
                 KladrStreet.ocatd],
                whereclause=and_(
                    Street.id.is_(None),
                    func.substring(KladrStreet.code, 16, 2) == u'00')).select_from(
                KladrStreet.table.outerjoin(
                    Street,
                    and_(func.substring(KladrStreet.code, 1, 2) == Street.codes,
                         func.substring(KladrStreet.code, 3, 3) == Street.coder,
                         func.substring(KladrStreet.code, 6, 3) == Street.codeg,
                         func.substring(KladrStreet.code, 9, 3) == Street.codep))).alias()

            q = select(
                [Town.id.label('town_id'),
                 q.c.name,
                 q.c.socr,
                 q.c.codes,
                 q.c.coder,
                 q.c.codeg,
                 q.c.codep,
                 q.c.codeu,
                 q.c.index,
                 q.c.gninmb,
                 q.c.uno,
                 q.c.ocatd]).select_from(
                q.join(
                    Town,
                    and_(q.c.codes == Town.codes,
                         q.c.coder == Town.coder,
                         q.c.codeg == Town.codeg,
                         q.c.codep == Town.codep))).alias()

            q = Street.table.insert().from_select(
                ['town_id', 'name', 'socr', 'codes', 'coder', 'codeg', 'codep', 'codeu', 'index', 'gninmb', 'uno',
                 'ocatd'],
                q)
            session.execute(q)

            q = select(
                [KladrDoma.name,
                 KladrDoma.numb,
                 KladrDoma.socr,
                 func.substring(KladrDoma.code, 1, 2).label('codes'),
                 func.substring(KladrDoma.code, 3, 3).label('coder'),
                 func.substring(KladrDoma.code, 6, 3).label('codeg'),
                 func.substring(KladrDoma.code, 9, 3).label('codep'),
                 func.substring(KladrDoma.code, 12, 4).label('codeu'),
                 func.substring(KladrDoma.code, 16, 4).label('coded'),
                 KladrDoma.index,
                 KladrDoma.gninmb,
                 KladrDoma.uno,
                 KladrDoma.ocatd],
                whereclause=House.id.is_(None)).select_from(
                KladrDoma.table.outerjoin(
                    House,
                    and_(func.substring(KladrDoma.code, 1, 2) == House.codes,
                         func.substring(KladrDoma.code, 3, 3) == House.coder,
                         func.substring(KladrDoma.code, 6, 3) == House.codeg,
                         func.substring(KladrDoma.code, 9, 3) == House.codep,
                         func.substring(KladrDoma.code, 12, 4) == House.codeu,
                         func.substring(KladrDoma.code, 16, 4) == House.coded,
                         KladrDoma.index == House.index))).alias()

            q = select(
                [Town.id.label('town_id'),
                 Street.id.label('street_id'),
                 q.c.name,
                 q.c.numb,
                 q.c.socr,
                 q.c.codes,
                 q.c.coder,
                 q.c.codeg,
                 q.c.codep,
                 q.c.codeu,
                 q.c.coded,
                 q.c.index,
                 q.c.gninmb,
                 q.c.uno,
                 q.c.ocatd]).select_from(
                q.join(
                    Town,
                    and_(q.c.codes == Town.codes,
                         q.c.coder == Town.coder,
                         q.c.codeg == Town.codeg,
                         q.c.codep == Town.codep)).join(
                    Street,
                    and_(q.c.codes == Street.codes,
                         q.c.coder == Street.coder,
                         q.c.codeg == Street.codeg,
                         q.c.codep == Street.codep,
                         q.c.codeu == Street.codeu))).alias()

            q = House.table.insert().from_select(
                ['town_id', 'street_id', 'name', 'numb', 'socr', 'codes', 'coder', 'codeg', 'codep', 'codeu', 'coded',
                 'index', 'gninmb', 'uno', 'ocatd'],
                q)
            session.execute(q)

        clean_temp(session)
        yield Refresh()
