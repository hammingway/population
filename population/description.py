# -*- coding: utf-8 -*-

from camelot.admin.action import Action
from camelot.core.utils import ugettext_lazy as _


class ProduceSchemaDescriprion(Action):
    verbose_name = _("Produce schema's description")

    def model_run(self, model_context):
        import codecs
        import sadisplay
        import model

        desc = sadisplay.describe(model.__dict__.values())

        with codecs.open('schema.plantuml', 'w', encoding='utf-8') as f:
            f.write(sadisplay.plantuml(desc))