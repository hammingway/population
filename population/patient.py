# -*- coding: utf-8 -*-

from datetime import datetime

from camelot.admin.action import Action
from camelot.core.utils import ugettext_lazy as _
from sqlalchemy import Unicode
from sqlalchemy.sql import select, update, join, outerjoin, func, and_, or_, expression, case

from model import (Town, Street, House, HousesRange, Division, Patient, PatientDBF, SimplePatient,
                   KeysList)
from population.addresses import Addresses
#from utils import LEN_FAM, LEN_IM, LEN_OTCH, val_new_cond, eq_ult, eql_ult
#from utils import iter_dbf
import application


class ImportPatients(Action):
    verbose_name = _('Import Patients')

    def model_run(self, model_context):
        from camelot.view.action_steps import (SelectFile,
                                               Refresh,
                                               FlushSession)
        from sqlalchemy.sql import delete
        from utils import mk_rus, mk_street, mk_town, mk_ces, mk_ss, is_town, iter_dbf

        select_archives = SelectFile('XBase Files (*.DBF);;All Files (*)')
        select_archives.single = True
        file_names = yield select_archives
        if not file_names:
            return

        pdtable = Patient.metadata.tables['patient_division__division_patient']

        def clean_stat(session):
            seq = (delete(pdtable),
                   delete(Patient.table))
            for i in seq:
                session.execute(i)

        # session = Session()
        session = model_context.session

        with session.begin():
            clean_stat(session)

            def mk_pint(val, alt=''):
                ival = -1
                try:
                    ival = int(val.replace(val.lstrip('1234567890'), ''))
                except ValueError:
                    pass
                try:
                    if ival < 1:
                        ival = int(alt.replace(alt.lstrip('1234567890'), ''))
                except ValueError:
                    pass
                return ival

            for i, r in iter_dbf(file_names[0]):
                p = Patient()
                p.n5 = r.n5
                p.n6 = r.n6
                p.n9 = r.n9
                p.n10 = r.n10
                p.n13 = mk_rus(r.n13.upper())
                n14, n15 = mk_rus(r.n14.upper()), mk_rus(r.n15.upper())
                p.n14 = n14
                p.n15 = n15
                n16 = mk_rus(r.n16.upper())
                p.n16 = n16
                p.n17 = mk_rus(r.n17.upper())
                p.n27 = r.n27
                p.fam = mk_rus(r.fam.upper())
                p.im = mk_rus(r.im.upper())
                p.ot = mk_rus(r.ot.upper())
                p.drd = r.dr_d
                p.pol = r.pol
                p.snils = r.snils
                p.smo_cod = r.smo_cod
                ces = mk_ces(mk_rus(r.ss_end.upper()))
                # ce, cs = mk_rus(r.cla_end.upper()), mk_rus(r.cla_soc.upper())
                se, ss = mk_rus(r.str_end.upper()), mk_rus(r.str_soc.upper())
                # if not ce:
                #    ce = n14 if n14 else n15
                # ce, cs = mk_town(ce, cs, ces)
                if not se:
                    se = n16
                if is_town(se):
                    ce, cs = mk_town(se, '', ces)
                    # ce, cs = mk_mkr(ce, cs, n15)
                    p.str_end = u''
                    p.str_soc = u''
                else:
                    ce, cs = mk_rus(r.cla_end.upper()), mk_rus(r.cla_soc.upper())
                    if not ce:  # or (n14 or n15) and (ce in (u'БОР', u'Г БОР', u'ГОРОД БОР')):
                        ce = n14 if n14 else n15
                    ce, cs = mk_town(ce, cs, ces)
                    # ce, cs = mk_mkr(ce, cs, n15)
                    se, ss = mk_street(se, ss)
                    p.str_end = se
                    ss = mk_ss(se, ss)
                    p.str_soc = ss

                p.cla_end = ce
                p.ss_end = ces
                p.cla_soc = cs

                p.dom = r.dom
                p.numb = mk_pint(r.dom, r.n17)
                p.kv = r.kv
                p.naiden = r.naiden
                # p.gs = r.gs
                # p.lpu_cod = r.lpu_cod
                # p.lpu = r.lpu
                p.skv_uch = r.skv_uch
                p.adres = mk_rus(r.adres.upper())
                if i % 200 == 0:
                    yield FlushSession(session)

            yield FlushSession(session)

        yield Refresh()


class LinkPatients(Action):
    verbose_name = _('Link Patients')

    def model_run(self, model_context):
        from camelot.view.action_steps import (Refresh,
                                               FlushSession)

        from camelot.view.action_steps import MessageBox
        from sqlalchemy.sql import delete
        from camelot.core.qt import QtGui

        pdtable = Patient.metadata.tables['patient_division__division_patient']
        hdtable = Division.metadata.tables['house_division__division_house']
        today = datetime.today()
        dmature = today.replace(year=today.year - 18)

        def clean_stat(session):
            seq = (delete(pdtable),
                   # delete(PatientDivision)
                   )
            for i in seq:
                session.execute(i)

        yield MessageBox(_(u'Do you want to link patients?'), QtGui.QMessageBox.Question, title=_(u'confirmation'))

        session = model_context.session

        with session.begin():
            clean_stat(session)

            q_a = select(
                [House.id.label('h_id'),
                 House.street_id,
                 House.name.label('h_name'),
                 House.numb.label('h_numb'),
                 Town.id.label('t_id'),
                 Town.name.label('t_name'),
                 Town.socr.label('t_socr')],
                whereclause=House.town_id == Town.id).with_for_update().alias()

            q_a = outerjoin(
                q_a, Street,
                q_a.c.street_id == Street.id).select().alias()

            q_a = select(
                [q_a.c.h_id,
                 q_a.c.h_name,
                 q_a.c.h_numb,
                 q_a.c.id.label('s_id'),
                 q_a.c.name.label('s_name'),
                 q_a.c.socr.label('s_socr'),
                 q_a.c.t_id,
                 q_a.c.t_name,
                 q_a.c.t_socr]).with_for_update().alias()

            q = update(
                Patient,
                whereclause=(
                    and_(func.lower(Patient.cla_end) == func.lower(q_a.c.t_name),
                         # Patient.cla_soc == q_a.c.t_socr,
                         func.lower(Patient.str_end) == func.lower(q_a.c.s_name),
                         func.lower(Patient.str_soc) == func.lower(q_a.c.s_socr),
                         func.lower(Patient.dom) == func.lower(q_a.c.h_name))),
                values={'house_id': q_a.c.h_id})
            session.execute(q)

            # без домов кладра
            q = select(
                [Division.spec,
                 HousesRange.division_id.label('hr_id'),
                 HousesRange.name.label('hr_name'),
                 HousesRange.cla_end.label('hr_cla_end'),
                 HousesRange.cla_soc.label('hr_cla_soc'),
                 HousesRange.str_end.label('hr_str_end'),
                 HousesRange.str_soc.label('hr_str_soc'),
                 HousesRange.nech_n.label('hr_nech_n'),
                 HousesRange.nech_e.label('hr_nech_e'),
                 HousesRange.chet_n.label('hr_chet_n'),
                 HousesRange.chet_e.label('hr_chet_e')],
                whereclause=and_(
                    HousesRange.division_id == Division.id,
                    HousesRange.division_id.isnot(None))
            ).with_for_update().alias()

            q_jp = join(
                q, Patient,
                and_(Patient.house_id == None,
                     func.lower(q.c.hr_name) == func.lower(Patient.cla_end),
                     # q.c.hr_cla_end == Patient.cla_end,
                     # q.c.hr_cla_soc == Patient.cla_soc,
                     or_(and_(q.c.hr_str_end.is_(None),
                              Patient.str_end.is_(None),
                              q.c.hr_str_soc.is_(None),
                              Patient.str_soc.is_(None)),
                         and_(or_(q.c.hr_str_end.isnot(None),
                                  Patient.str_end.isnot(None),
                                  q.c.hr_str_soc.isnot(None),
                                  Patient.str_soc.isnot(None)),
                              func.lower(q.c.hr_str_end) == func.lower(Patient.str_end),
                              func.lower(q.c.hr_str_soc) == func.lower(Patient.str_soc))),
                     or_(and_(Patient.numb % 2 == 1,
                              Patient.numb >= q.c.hr_nech_n,
                              Patient.numb <= q.c.hr_nech_e),
                         and_(Patient.numb % 2 == 0,
                              Patient.numb >= q.c.hr_chet_n,
                              Patient.numb <= q.c.hr_chet_e)),
                     or_(and_(q.c.spec == u'взрослые',
                              Patient.drd < dmature),
                         and_(q.c.spec == u'венеролог',
                              # Patient.pol == u'ж',
                              Patient.drd < dmature),
                         and_(q.c.spec == u'педиатр',
                              Patient.drd >= dmature))
                     )).select().alias()

            q = select([q_jp.c.id.label('paient_id'),
                        q_jp.c.hr_id.label('division_id')],
                       distinct=True).with_for_update().alias()

            q = pdtable.insert().from_select(
                ['patient_id', 'division_id'],
                q)
            session.execute(q)

            # с домами кладра
            q = select(
                [Patient.id.label('patient_id'),
                 Division.id.label('division_id')],
                whereclause=and_(
                    Patient.house_id == hdtable.c.house_id,
                    hdtable.c.division_id == Division.id,
                    or_(and_(Division.spec == u'взрослые',
                             Patient.drd < dmature),
                        and_(Division.spec == u'венеролог',
                             # Patient.pol == u'ж',
                             Patient.drd < dmature),
                        and_(Division.spec == u'педиатр',
                             Patient.drd >= dmature))
                )).with_for_update()

            q = pdtable.insert().from_select(
                ['patient_id', 'division_id'],
                q)
            session.execute(q)

        yield FlushSession(session)
        yield Refresh()


class UpdatePatients(Action):
    verbose_name = _('Update Patients')

    def model_run(self, model_context):
        from camelot.view.action_steps import (SelectFile,
                                               Refresh,
                                               FlushSession)
        import dbf
        from sqlalchemy.sql import delete
        from utils import mk_rus, mk_street, mk_town, mk_ces, mk_ss, is_town, iter_dbf, val_new_cond, eql_ult

        select_archives = SelectFile('XBase Files (*.DBF);;All Files (*)')
        select_archives.single = True
        file_names = yield select_archives
        if not file_names:
            return

        def clean_temp(session):
            seq = (delete(PatientDBF.table),
                   )
            for i in seq:
                session.execute(i)

        session = model_context.session

        clean_temp(session)

        def mk_pint(val, alt=''):
            ival = -1
            try:
                ival = int(val.replace(val.lstrip('1234567890'), ''))
            except ValueError:
                pass
            try:
                if ival < 1:
                    ival = int(alt.replace(alt.lstrip('1234567890'), ''))
            except ValueError:
                pass
            return ival

        tbl = dbf.Table(file_names[0], codepage='cp866')
        usable = set(PatientDBF.table.columns.keys()) - {'id'}
        common = tuple(usable & set(tbl._meta.fields))
        torupcase = {
            'n13', 'n14', 'n15', 'n16', 'fam', 'im', 'ot', 'ss_end', 'str_end', 'str_soc', 'cla_end', 'cla_soc', 'adres'
        }
        strcols = set(i.name for i in PatientDBF.table.columns if isinstance(i.type, Unicode)) - {'id'}
        for ir, rec in iter_dbf(tbl):
            r = dict.fromkeys(usable)
            for i in common:
                a = getattr(rec, i)
                if i in torupcase and a:
                    a = mk_rus(a.upper())
                r[i] = a

            ces = mk_ces(r['ss_end'])
            se, ss = r['str_end'], r['str_soc']
            if not se:
                se = r['n16']
            if is_town(se):
                ce, cs = mk_town(se, '', ces)
                # r['str_end'] = u''
                # r['str_soc'] = u''
            else:
                ce, cs = r['cla_end'], r['cla_soc']
                if not ce:
                    ce = r['n14'] if r['n14'] else r['n15']
                ce, cs = mk_town(ce, cs, ces)
                se, ss = mk_street(se, ss)
                r['str_end'] = se
                ss = mk_ss(se, ss)
                r['str_soc'] = ss
            r['cla_end'] = ce
            r['ss_end'] = ces
            r['cla_soc'] = cs

            p = PatientDBF()
            for k, v in r.iteritems():
                if k in strcols and not v:
                    continue
                setattr(p, k, v)

            if ir % 200 == 0:
                yield FlushSession(session)

        yield FlushSession(session)

        with session.begin():
            # q_e = select(
            #     [Patient.id.label('exists_id')]
            # ).select_from(
            #     Patient.table.join(
            #         PatientDBF,
            #         and_(
            #             Patient.fam == PatientDBF.fam,
            #             Patient.im == PatientDBF.im,
            #             Patient.ot == PatientDBF.ot,
            #             Patient.drd == PatientDBF.drd)
            #     )
            # ).alias()

            patfields = {
                'n5', 'n6', 'n9', 'n10', 'n13', 'n14', 'n15', 'n16', 'n17', 'n27', 'fam', 'im', 'ot', 'drd', 'pol',
                'snils', 'smo_cod', 'cla_end', 'ss_end', 'cla_soc', 'str_end', 'str_soc', 'dom', 'numb', 'kv', 'naiden',
                'lnkd', 'skv_uch', 'adres'
            }

            # patfields = {
            #     'lnkd'
            # }

            upatfields = patfields - {'fam', 'im', 'ot', 'drd'}
            updatvals = {attr: val_new_cond(attr, Patient, PatientDBF) for attr in upatfields}
            updatvals['n5'] = case([(func.len(PatientDBF.n6) == 16, None)], else_=Patient.n5)
            # q = update(
            #     Patient,
            #     whereclause=and_(
            #         # Patient.id.in_(q_e),
            #         # Patient.n5 == PatientDBF.n5,
            #         # Patient.n6 == PatientDBF.n6,
            #         Patient.fam == PatientDBF.fam,
            #         Patient.im == PatientDBF.im,
            #         eq_ult(Patient.ot, PatientDBF.ot),
            #         Patient.drd == PatientDBF.drd  # ,
            #         # Patient.snils == PatientDBF.snils
            #     ),
            #     values=updatvals
            # )
            # session.execute(q)

            q = select(
                [getattr(PatientDBF, attr) for attr in patfields],
                whereclause=Patient.id.is_(None),
                distinct=True
            ).select_from(
                PatientDBF.table.outerjoin(
                    Patient,
                    and_(
                        func.lower(PatientDBF.fam) == func.lower(Patient.fam),
                        func.lower(PatientDBF.im) == func.lower(Patient.im),
                        eql_ult(PatientDBF.ot, Patient.ot),
                        PatientDBF.drd == Patient.drd)
                )
            ).alias()
            q = Patient.table.insert().from_select(patfields, q)
            session.execute(q)

        yield FlushSession(session)
        yield Refresh()


class LoadSimplePatients(Action):
    verbose_name = _('Load Simple Patients')

    def model_run(self, model_context):
        from camelot.view.action_steps import (SelectFile,
                                               Refresh,
                                               FlushSession)
        from sqlalchemy.sql import delete
        from utils import mk_rus, iter_dbf

        select_archives = SelectFile('XBase Files (*.DBF);;All Files (*)')
        select_archives.single = True
        file_names = yield select_archives
        if not file_names:
            return

        def clean_temp(session):
            seq = (delete(SimplePatient.table),
                   )
            for i in seq:
                session.execute(i)

        session = model_context.session

        clean_temp(session)

        with session.begin():

            for i, r in iter_dbf(file_names[0]):
                p = SimplePatient()
                p.fam = mk_rus(r.fam.upper())
                p.im = mk_rus(r.im.upper())
                if hasattr(r, 'ot'):
                    p.ot = mk_rus(r.ot.upper())
                elif hasattr(r, 'otch'):
                    p.ot = mk_rus(r.otch.upper())
                else:
                    raise Exception('Wrong format')
                if hasattr(r, 'drd'):
                    p.drd = r.drd
                elif hasattr(r, 'dr'):
                    p.drd = r.dr
                else:
                    raise Exception('Wrong format')
                if i % 200 == 0:
                    yield FlushSession(session)

            yield FlushSession(session)

        yield Refresh()


class Deduplication(Action):
    verbose_name = _('Deduplication of patients')

    def model_run(self, model_context):
        from camelot.view.action_steps import (Refresh,
                                               FlushSession)
        from sqlalchemy.sql import delete
        from utils import LEN_FAM, LEN_IM, LEN_OTCH, eq_ult, eql_ult

        session = model_context.session

        with session.begin():
            #KeysList.update_or_create({})
            #session.execute(delete(KeysList))

            patfields = {
                'n5', 'n6', 'n9', 'n10', 'n13', 'n14', 'n15', 'n16', 'n17', 'n27', 'fam', 'im', 'ot', 'drd', 'pol',
                'snils', 'smo_cod', 'cla_end', 'ss_end', 'cla_soc', 'str_end', 'str_soc', 'dom', 'numb', 'kv', 'naiden',
                'unld', 'lnkd', 'skv_uch', 'adres', 'house_id', 'mkb_id'
            }

            # Уникальность по номеру полиса + началам ФИО

            q_g = select(
                [func.max(getattr(Patient, i)).label(i) for i in patfields] +
                [func.substring(func.max(Patient.fam), 1, LEN_FAM).label('fam_s'),
                 func.substring(func.max(Patient.im), 1, LEN_IM).label('im_s'),
                 func.substring(func.max(Patient.ot), 1, LEN_OTCH).label('ot_s'), ],
                whereclause=Patient.n6.isnot(None)
            ) \
                .group_by(
                Patient.n5,
                Patient.n6,
                func.substring(Patient.fam, 1, LEN_FAM),
                func.substring(Patient.im, 1, LEN_IM),
                func.substring(Patient.ot, 1, LEN_OTCH),
                Patient.drd
            ) \
                .having(func.count(Patient.id) > 1).with_for_update().alias()

            q = select(
                [Patient.id]
            ).select_from(
                Patient.table.join(
                    q_g,
                    and_(
                        eq_ult(q_g.c.n5, Patient.n5),
                        eq_ult(q_g.c.n6, Patient.n6),
                        func.lower(q_g.c.fam_s) == func.lower(func.substring(Patient.fam, 1, LEN_FAM)),
                        func.lower(q_g.c.im_s) == func.lower(func.substring(Patient.im, 1, LEN_IM)),
                        eql_ult(q_g.c.ot_s, func.substring(Patient.ot, 1, LEN_OTCH)),
                        q_g.c.drd == Patient.drd
                    )
                )
            ).with_for_update().alias()

            # сначала выделяем ключи дублирующихся
            q = KeysList.table.insert().from_select([q.c.id], q)
            session.execute(q)

            # затем добавляем композитные данные
            q = select([getattr(q_g.c, i) for i in patfields]).alias()
            q = Patient.table.insert().from_select(patfields, q)
            session.execute(q)

            # только вконце удаляем дублирующиеся
            q = select([KeysList.id]).alias()
            q = delete(Patient, whereclause=Patient.id.in_(q))
            session.execute(q)

            # Уникальность по началам ФИО и адресу

            session.execute(delete(KeysList))

            q_g = select(
                [func.max(getattr(Patient, i)).label(i) for i in patfields] +
                [func.substring(func.max(Patient.fam), 1, LEN_FAM).label('fam_s'),
                 func.substring(func.max(Patient.im), 1, LEN_IM).label('im_s'),
                 func.substring(func.max(Patient.ot), 1, LEN_OTCH).label('ot_s'),
                 ],
                whereclause=and_(
                    Patient.cla_end.isnot(None),
                    Patient.numb.isnot(None),
                    Patient.numb > 0
                )
            ) \
                .group_by(
                func.substring(Patient.fam, 1, LEN_FAM),
                func.substring(Patient.im, 1, LEN_IM),
                func.substring(Patient.ot, 1, LEN_OTCH),
                Patient.drd,
                Patient.cla_soc,
                Patient.cla_end,
                Patient.str_end,
                Patient.str_soc,
                Patient.numb,
                Patient.kv,
            ) \
                .having(func.count(Patient.id) > 1).with_for_update().alias()

            q = select(
                [Patient.id]
            ).select_from(
                Patient.table.join(
                    q_g,
                    and_(
                        func.lower(q_g.c.fam_s) == func.lower(func.substring(Patient.fam, 1, LEN_FAM)),
                        func.lower(q_g.c.im_s) == func.lower(func.substring(Patient.im, 1, LEN_IM)),
                        eql_ult(q_g.c.ot_s, func.substring(Patient.ot, 1, LEN_OTCH)),
                        q_g.c.drd == Patient.drd,
                        func.lower(q_g.c.cla_soc) == func.lower(Patient.cla_soc),
                        func.lower(q_g.c.cla_end) == func.lower(Patient.cla_end),
                        or_(
                            and_(
                                func.lower(q_g.c.str_soc) == func.lower(Patient.str_soc),
                                func.lower(q_g.c.str_end) == func.lower(Patient.str_end)
                            ),
                            and_(
                                q_g.c.str_soc.is_(None),
                                Patient.str_soc.is_(None),
                                q_g.c.str_end.is_(None),
                                Patient.str_end.is_(None)
                            )
                        ),
                        q_g.c.numb == Patient.numb,
                        eql_ult(q_g.c.kv, Patient.kv)
                    )
                )
            ).with_for_update().alias()

            # сначала выделяем ключи дублирующихся
            q = KeysList.table.insert().from_select([q.c.id], q)
            session.execute(q)

            # затем добавляем композитные данные
            q = select([getattr(q_g.c, i) for i in patfields]).alias()
            q = Patient.table.insert().from_select(patfields, q)
            session.execute(q)

            # только вконце удаляем дублирующиеся
            q = select([KeysList.id]).alias()
            q = delete(Patient, whereclause=Patient.id.in_(q))
            session.execute(q)

        yield FlushSession(session)
        yield Refresh()


# Пока работает на клиенте
class FillAddressesLacks(Action):
    verbose_name = _('Fill addresses lacks')

    def model_run(self, model_context):
        from camelot.view.action_steps import (Refresh,
                                               FlushSession)
        from sqlalchemy.sql import delete

        session = model_context.session

        with session.begin():
            session.execute(delete(PatientDBF))

            application.the_application.addresses.streets_import(session)
            application.the_application.addresses.towns_import(session)
            application.the_application.addresses.patients_load(session)
            for i in application.the_application.addresses.addresses_fill(session):
                yield i

            q = update(
                Patient,
                whereclause=Patient.id == PatientDBF.id,
                values={
                    'cla_end': PatientDBF.cla_end,
                    'cla_soc': PatientDBF.cla_soc,
                    'str_end': PatientDBF.str_end,
                    'str_soc': PatientDBF.str_soc,
                    'dom': PatientDBF.dom,
                    'numb': PatientDBF.numb,
                    'kv': PatientDBF.kv
                }
            )
            session.execute(q)

        yield FlushSession(session)
        yield Refresh()


# Пока работает на клиенте
class ExtractAddress(Action):
    verbose_name = _('Extract address')

    def model_run(self, model_context):
        from camelot.view.action_steps import (FlushSession)

        session = model_context.session

        current = model_context.get_object()
        if current is None:
            return
        application.the_application.addresses.streets_import(session)
        application.the_application.addresses.towns_import(session)
        addr = application.the_application.addresses.addr_normalize(current.adres)
        if not addr:
            return
        current.cla_end = addr['cla_end']
        current.cla_soc = addr['cla_soc']
        current.str_end = addr['str_end']
        current.str_soc = addr['str_soc']
        current.dom = addr['numb']
        current.kv = addr['kv']
        current.numb = int(addr['numb'])

        yield FlushSession(session)
