# -*- coding: utf-8 -*-

import dbf
import types
import traceback
import re
from functools import wraps
from itertools import chain
from sqlalchemy.sql import func, case, and_, or_
from camelot.core.exception import CancelRequest

import logging

logger = logging.getLogger('main')


class Translator(object):
    AT_START = 1
    AT_END = 2

    def __init__(self, dlist, mode=None):
        self.rep = {re.escape(k): v for k, v in dlist}
        if mode == self.AT_START:
            self.pattern = re.compile("|".join(ur'\A%s' % k for k in self.rep.keys()), re.U)
        elif mode == self.AT_END:
            self.pattern = re.compile("|".join(u'%s$' % k for k in self.rep.keys()), re.U)
        else:
            self.pattern = re.compile("|".join(self.rep.keys()), re.U)

    def translate(self, text):
        return self.pattern.sub(lambda m: self.rep[re.escape(m.group(0))], text)

    def replace(self, text, string=u''):
        return self.pattern.sub(string, text)

    def suggest(self, text):
        m = self.pattern.search(text)
        if m is not None:
            return m.group(0), self.rep[m.group(0)]
        return None


def mk_startendcheck(dlist):
    return re.compile("|".join(
        chain(
            (ur'\A%s ' % k for k in dlist),
            (u' %s$' % k for k in dlist)
        )
    ), re.U)


_RUS_REPL = ((u'е', 'e'), (u'т', 't'), (u'о', 'o'), (u'р', 'p'), (u'а', 'a'), (u'н', 'h'), (u'к', 'k'), (u'х', 'x'),
             (u'с', 'c'), (u'в', 'b'), (u'м', 'm'), (u'Е', 'E'), (u'Т', 'T'), (u'О', 'O'), (u'Р', 'P'), (u'А', 'A'),
             (u'Н', 'H'), (u'К', 'K'), (u'Х', 'X'), (u'С', 'C'), (u'В', 'B'), (u'М', 'M'))

RUS_REPL = Translator(_RUS_REPL)
ENG_REPL = Translator((r, l) for l, r in _RUS_REPL)

_TOWN_FULL = ((u'СОРМОВСКИЙ ПРОЛЕТАРИЙ', u'СВХ СОРМОВСКИЙ ПРОЛЕТАРИЙ (ЛИНД С/С)', u'СВХ СОРМОВСКИЙ ПРОЛЕТАРИЙ'),
              (u'КАНТАУРОВО', u'КАНТАУРОВО (КАНТАУРОВСКИЙ С/С)', u'КАНТАУРОВО'),
              (u'РЕДЬКИНО', u'РЕДЬКИНО (РЕДЬКИНСКИЙ С/С)', u'РЕДЬКИНО'),
              (u'СИТНИКИ', u'СИТНИКИ (СИТНИКОВСКИЙ С/С)', u'СИТНИКИ'))

TOWN_FULL = Translator((l, r) for l, r, _ in _TOWN_FULL)

_TOWN_SOCR = ((u' ГОРОД ', u'Г'),
              (u'ЖИЛОЙ РАЙОН', u'МКР'),
              (u'МКР', u'МКР'),
              (u'М/Р', u'МКР'),
              (u'Ж/Р', u'МКР'),
              (u'МИКРОРАЙОН', u'МКР'),
              (u'РАБОЧИЙ ПОСЕЛОК', u'РП'),
              (u'ПОСЕЛОК', u'РП'))

TOWN_SOCR = Translator(_TOWN_SOCR)
TOWN_SOCR_ST_EN = mk_startendcheck(e for _, e in _TOWN_SOCR)

_TOWN_START = ((u'ГОРОД ', u'Г'),
               (u'Д ', u'Д'),
               (u'Д.', u'Д'),
               (u'Г ', u'Г'),
               (u'Г.', u'Г'),
               (u'С ', u'С'),
               (u'С.', u'С'),
               (u'П ', u'П'),
               (u'П.', u'П'))

TOWN_START = Translator(_TOWN_START, Translator.AT_START)

_TOWN_END = ((u' ГОРОД', u'Г'),
             (u' Д', u'Д'),
             (u' Д.', u'Д'),
             (u' Г', u'Г'),
             (u' Г.', u'Г'),
             (u' С', u'С'),
             (u' С.', u'С'),
             (u' П', u'П'),
             (u' П.', u'П'))

TOWN_END = Translator(_TOWN_END, Translator.AT_END)

_STREET_SOCR = ((u'УЛИЦА', u'УЛ'),
                (u'ПЕРЕУЛОК', u'ПЕР'))

STREET_SOCR = Translator(_STREET_SOCR)

_STREET_START = ((u'УЛ ', u'УЛ'),
                 (u'УЛ.', u'УЛ'),
                 (u'ПЕР ', u'ПЕР'),
                 (u'ПЕР.', u'ПЕР'))

_STREET_END = ((u' УЛ', u'УЛ'),
               (u' УЛ.', u'УЛ'),
               (u' ПЕР', u'ПЕР'),
               (u' ПЕР.', u'ПЕР'))

_SS_REPLACE = ((u'СЕЛЬСОВЕТ', u'С/С'),
               (u'ПАМЯТЬ ПАРИЖСКОЙ КОММУНЫ', u'ППК'))

LEN_FAM = 10
LEN_IM = 9
LEN_OTCH = 13


def is_number(v):
    try:
        int(v)
        return True
    except ValueError:
        pass
    return False


def mk_rus(s):
    return ENG_REPL.translate(s)


def mk_eng(s):
    return RUS_REPL.translate(s)


def mk_town(end, soc, ss, need_clean=False):
    if end is None:
        if need_clean:
            return end, soc, end
        return end, soc

    end = end.upper()
    sugg = TOWN_FULL.suggest(end)
    if sugg is not None:
        end = sugg[1]
    sugg = TOWN_SOCR.suggest(end)
    if sugg is not None:
        soc = sugg[1]
        end = end.replace(sugg[0], '')
    end = end.strip()
    sugg = TOWN_START.suggest(end)
    if sugg is not None:
        soc = sugg[1]
        end = end[len(sugg[0]):].lstrip()
    sugg = TOWN_END.suggest(end)
    if sugg is not None:
        soc = sugg[1]
        end = end[:-len(sugg[0])].rstrip()
    end.replace('  ', ' ')
    end.replace('  ', ' ')
    if ss:
        ef = end.split('(')[0].strip()
        end = u'%s (%s)' % (ef, ss)
        if len(end) > 40:
            sp = ss.split(' ')
            end = u'%s (%s %s)' % (ef, sp[0][:4], sp[-1])
        if need_clean:
            return end, soc, ef.rstrip()

    if soc == u'МКР':
        if is_number(end):
            end = u'%s-Й' % end
        else:
            endl = end.split(u' ')
            if is_number(endl[-1]):
                end = u'%s-%s' % (u' '.join(endl[:-1]), endl[-1])

    if need_clean:
        return end, soc, end
    return end, soc


def mk_street(end, soc):
    if end is None:
        return end, soc

    end = end.upper()
    sugg = STREET_SOCR.suggest(end)
    if sugg is not None:
        soc = sugg[1]
    end = STREET_SOCR.replace(end)
    end = end.strip()
    for s, r in STREET_START:
        if end.startswith(s):
            soc = r
            end = end[len(s):].lstrip()
            break
    for s, r in STREET_END:
        if end.endswith(s):
            soc = r
            end = end[:-len(s)].rstrip()
            break
    end.replace('  ', ' ')
    end.replace('  ', ' ')
    return end, soc


def mk_mkr(end, soc, pr):
    if pr and (soc == u'Г' or u'БОР' in end):
        pr, prs = mk_town(pr, None, None)
        return pr, prs
    return end, soc


def mk_ces(ss):
    if ss is not None:
        for s, r in SS_REPLACE:
            ss = ss.replace(s, r, 1)
        return ss


def mk_ss(se, ss):
    if se:
        if not ss:
            if u'СКИЙ' in se:
                ss = u'ПЕР'
            else:
                ss = u'УЛ'
    else:
        ss = None
    return ss


def is_town(st):
    if st is None:
        return False

    sugg = TOWN_SOCR.suggest(st)
    if sugg is not None:
        cl = st.replace(sugg[0], '').strip()
        if not cl or cl == u'УЛ':
            return False
        return True
    if TOWN_SOCR_ST_EN.search(st) is not None:
        return True
    if TOWN_START.suggest(st) is not None:
        return True
    if TOWN_END.suggest(st) is not None:
        return True
    return False


################################################################################


class DBFRecordProxy(object):
    def __init__(self, r):
        self.record = r

    def __getattr__(self, item):
        v = getattr(self.record, item)
        if isinstance(v, (str, unicode)):
            return v.strip()
        return v


def iter_dbf(path_or_table):
    if isinstance(path_or_table, (str, unicode)):
        tbl = dbf.Table(path_or_table, codepage='cp866')
    else:
        tbl = path_or_table
    tbl.open()
    try:
        for i, r in enumerate(tbl):
            if dbf.is_deleted(r):
                continue
            yield i, DBFRecordProxy(r)
    finally:
        tbl.close()


def val_new_cond(attr, old, new):
    """Set new value if it present
    """
    return case([(getattr(new, attr) != None, getattr(new, attr))], else_=getattr(old, attr))


def val_old_cond(attr, old, new):
    """Leave old value if it present
    """
    return case([(getattr(old, attr) != None, getattr(old, attr))], else_=getattr(new, attr))


# def empty_ult(a):
#     """Ultimativelly empty
#     """
#     return or_(a == u'', a.is_(None))


# def nempty_ult(a):
#     """Ultimativelly non empty
#     """
#     return and_(a != u'', a.isnot(None))


def eq_ult(l, r):
    """Ultimativelly equals
    """
    return or_(l == r, and_(l.is_(None), r.is_(None)))


def eql_ult(l, r):
    """Ultimativelly equals in lower case
    """
    return or_(func.lower(l) == func.lower(r), and_(l.is_(None), r.is_(None)))


def fam_eq(left, right):
    return func.lower(func.substring(getattr(left, 'fam'), 1, LEN_FAM)) \
           == func.lower(func.substring(getattr(right, 'fam'), 1, LEN_FAM))


def im_eq(left, right):
    return func.lower(func.substring(getattr(left, 'im'), 1, LEN_IM)) \
           == func.lower(func.substring(getattr(right, 'im'), 1, LEN_IM))


def ot_eq(left, right):
    return eql_ult(func.substring(getattr(left, 'ot'), 1, LEN_OTCH),
                   func.substring(getattr(right, 'ot'), 1, LEN_OTCH))


_NOTEXCEPTIONS = (StopIteration, GeneratorExit, KeyboardInterrupt, SystemExit, CancelRequest)


def hide_exceptions(*exceptions):
    def deco(func):
        def generators_wrapper(tested_generator):
            val = None
            while True:
                try:
                    val = yield tested_generator.send(val)
                except exceptions:
                    pass

        @wraps(func)
        def tested_function(*args, **kwargs):
            fresult = None
            try:
                fresult = func(*args, **kwargs)
            except exceptions:
                pass
            if isinstance(fresult, types.GeneratorType):
                return generators_wrapper(fresult)
            return fresult

        return tested_function

    return deco


def log_exceptions(func):
    def generators_wrapper(tested_generator):
        val = None
        while True:
            try:
                val = yield tested_generator.send(val)
            except BaseException as e:
                if not isinstance(e, _NOTEXCEPTIONS):
                    logger.error(traceback.format_exc())
                raise e

    @wraps(func)
    def tested_function(*args, **kwargs):
        try:
            fresult = func(*args, **kwargs)
        except BaseException as e:
            if not isinstance(e, _NOTEXCEPTIONS):
                logger.error(traceback.format_exc())
            raise e
        if isinstance(fresult, types.GeneratorType):
            return generators_wrapper(fresult)
        return fresult

    return tested_function
