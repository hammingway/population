from camelot.view.art import Icon
from camelot.view.action_steps import OpenFile, MessageBox
from camelot.admin.application_admin import ApplicationAdmin
from camelot.admin.action import Action, application_action, list_action
from camelot.admin.section import Section
from camelot.core.qt import Qt, QtCore, QtWidgets
from camelot.core.utils import ugettext, ugettext_lazy as _
from model import (Kladr, KladrStreet, KladrDoma, KladrAltnames, Town, Street, House, DivisionDBF, Division,
                   HousesRange, Patient, Diagnose, RequestHdr, SimplePatient)
from view import (OrphanedDivision, OrphanedPatient, PatientWithDiagn, PatientDispanser,
                  PatientExists, PatientAssign, PatientFromList, PatientDuplications, PatientProf, PolisesDuplications,
                  LinkedPatient) # , AllSimple
from kladr import ImportKladr, UpdateKladr
from division import ImportDivisions, LinkDivisions
from patient import ImportPatients, LinkPatients, UpdatePatients, LoadSimplePatients
from references import ImportDiagnoses
from exporter import ExportXbase
from description import ProduceSchemaDescriprion
from patient import FillAddressesLacks
import os
import six

LOGNAME = 'runtime.population.camelot.log'
LOGNAME_STDERR = 'stderr.population.camelot.log'


class MyApplicationAdmin(ApplicationAdmin):
    name = 'population'
    application_url = None
    help_url = None
    author = 'mrprint'
    domain = 'org'

    def get_sections(self):
        from camelot.model.memento import Memento
        from camelot.model.i18n import Translation
        return [Section(_('My classes'),
                        self,
                        Icon('tango/22x22/places/folder.png'),
                        items=[Patient, Division, Town, Street, House, HousesRange, Diagnose,
                               RequestHdr]),
                Section(_('Divisions reports'),
                        self,
                        Icon('tango/22x22/status/mail-attachment.png'),
                        items=[OrphanedDivision, OrphanedPatient, LinkedPatient]),
                Section(_('Dispanserisation reports'),
                        self,
                        Icon('tango/22x22/apps/system-users.png'),
                        items=[PatientDispanser, PatientExists, PatientProf]),
                Section(_('Other reports'),
                        self,
                        Icon('tango/22x22/places/folder-saved-search.png'),
                        items=[PatientWithDiagn, PatientAssign, PatientFromList,
                               PatientDuplications, PolisesDuplications]),
                # Section(_('Temporaries'),
                #         self,
                #         Icon('tango/22x22/apps/system-users.png'),
                #         items=[SimplePatient]),
                Section(_('Configuration'),
                        self,
                        Icon('tango/22x22/categories/preferences-system.png'),
                        items=[Memento, Translation])
                ]

    def get_main_menu(self):
        from camelot.admin.menu import Menu

        return [Menu(_('&File'),
                     [application_action.Backup(),
                      application_action.Restore(),
                      None,
                      Menu(_('Export To'),
                           self.export_actions),
                      Menu(_('Import From'),
                           [list_action.ImportFromFile()]),
                      None,
                      application_action.Exit(),
                      ]),
                Menu(_('&Edit'),
                     self.edit_actions + [
                         None,
                         list_action.SelectAll(),
                         None,
                         list_action.ReplaceFieldContents(),
                         None,
                         LinkDivisions(),
                         LinkPatients(),
                         None,
                         LoadSimplePatients(),
                     ]),
                Menu(_('View'),
                     [application_action.Refresh(),
                      Menu(_('Go To'), self.change_row_actions)])] + \
               ([Menu(_('Admin'),
                     [ImportKladr(),
                      ImportDivisions(),
                      ImportPatients(),
                      ImportDiagnoses(),
                      None,
                      UpdateKladr(),
                      FillAddressesLacks(),
                      None,
                      UpdatePatients(),
                      ])] if self.app_args.admin else []) + \
                [Menu(_('&Help'),
                     self.help_actions + [
                         OpenLog(),
                         ProduceSchemaDescriprion(),
                         None,
                         ShowAbout()])
                ]

    def get_toolbar_actions(self, toolbar_area):
        if toolbar_area == Qt.TopToolBarArea:
            return self.edit_actions + self.change_row_actions + \
                   self.export_actions + [ExportXbase()] + self.help_actions

    def get_related_toolbar_actions(self, toolbar_area, direction):
        if toolbar_area == Qt.RightToolBarArea and direction == 'onetomany':
            return [list_action.AddNewObject(),
                    list_action.DeleteSelection(),
                    list_action.DuplicateSelection(),
                    list_action.ExportSpreadsheet(),
                    ExportXbase(), ]
        if toolbar_area == Qt.RightToolBarArea and direction == 'manytomany':
            return [list_action.AddExistingObject(),
                    list_action.RemoveSelection(),
                    list_action.ExportSpreadsheet(),
                    ExportXbase(), ]

    def get_translator(self):
        translators = super(MyApplicationAdmin, self).get_translator()
        locale_name = QtCore.QLocale().name()
        kladr_translator = self._load_translator_from_file('population',
                                                           os.path.join('%s/LC_MESSAGES/' % locale_name, 'messages'),
                                                           'translations/')
        if kladr_translator:
            translators.append(kladr_translator)
        return translators


class ShowAbout(Action):
    """Show the about dialog with the content returned by the
    :meth:`ApplicationAdmin.get_about` method
    """

    verbose_name = _('&About')
    icon = Icon('tango/16x16/mimetypes/application-certificate.png')
    tooltip = _("Show the application's About box")

    def gui_run(self, gui_context):

        def get_about():
            import datetime
            from camelot.core import license
            today = datetime.date.today()
            return """<b>Population</b><br/>
                      this software built on top of the Camelot framework
                      <p>
                      Copyright &copy; hammingway.
                      </p>
                      <p>
                      Copyright &copy; 2007-%s Conceptive Engineering.
                      All rights reserved.
                      </p>
                      <p>
                      %s
                      </p>
                      <p>
                      http://www.python-camelot.com<br/>
                      http://www.conceptive.be
                      </p>
                      """ % (today.year, license.license_type)

        abtmsg = get_about()
        QtWidgets.QMessageBox.about(gui_context.workspace,
                                    ugettext('About'),
                                    six.text_type(abtmsg))


class OpenTextStream(OpenFile):
    def __init__(self, stream, suffix='.txt'):
        import os
        import tempfile

        file_descriptor, file_name = tempfile.mkstemp(suffix=suffix)
        output_stream = os.fdopen(file_descriptor, 'w')
        output_stream.writelines(('%s' + os.linesep) % l.rstrip('\n') for l in stream.readlines())
        output_stream.close()
        super(OpenTextStream, self).__init__(file_name)


class OpenLog(Action):
    verbose_name = _('Open log')

    def model_run(self, model_context):
        from camelot.core.qt import QtGui
        import os

        try:
            with open(os.path.expanduser('~/%s' % LOGNAME_STDERR), 'r') as l:
                yield OpenTextStream(l)
        except Exception, e:
            yield MessageBox(_('Can\'t open logfile!'), QtGui.QMessageBox.Critical)
