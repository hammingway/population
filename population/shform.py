# -*- coding: utf-8 -*-

from camelot.admin.action import Action
from camelot.admin.object_admin import ObjectAdmin
from camelot.core.utils import ugettext_lazy as _
from camelot.view.action_steps import (ChangeObject, FlushSession, UpdateProgress, UpdateObject)
from camelot.view.controls import delegates
from camelot.view.forms import HBoxForm, WidgetOnlyForm

import application
import model


class AddressSel(Action):
    verbose_name = _('Check address')

    def model_run(self, model_context):
        obj = model_context.get_object()
        obj.use_cla_end = True
        obj.use_cla_soc = True
        obj.use_str_end = True
        obj.use_str_soc = True
        obj.use_dom = True
        obj.use_numb = True
        obj.use_kv = True
        yield UpdateObject(obj)


class CleanSel(Action):
    verbose_name = _('Clean selection')

    def model_run(self, model_context):
        obj = model_context.get_object()
        obj.use_nothing()
        yield UpdateObject(obj)


class PatientShape(object):
    def __init__(self):
        self.house = None
        self.pol = None
        self.cla_end = None
        self.cla_soc = None
        self.str_end = None
        self.str_soc = None
        self.dom = None
        self.numb = None
        self.kv = None
        self.skv_uch = None
        self.use_nothing()

    def use_nothing(self):
        self.use_house = False
        self.use_pol = False
        self.use_cla_end = False
        self.use_cla_soc = False
        self.use_str_end = False
        self.use_str_soc = False
        self.use_dom = False
        self.use_numb = False
        self.use_kv = False
        self.use_skv_uch = False

    class Admin(ObjectAdmin):
        verbose_name = _('Patient shape')
        verbose_name_plural = _('Patient shapes')
        field_attributes = {'pol':
                                {'name': u'Пол',
                                 'choices': lambda o: [(u'м', _('Male')),
                                                       (u'ж', _('Female')),
                                                       (None, _('Absent'))],
                                 'delegate': delegates.PlainTextDelegate,
                                 'editable': True},
                            'cla_end':
                                {'name': u'Н.Пункт',
                                 'delegate': delegates.PlainTextDelegate,
                                 'editable': True},
                            'cla_soc':
                                {'name': u'Тип Н.П.',
                                 'delegate': delegates.PlainTextDelegate,
                                 'editable': True},
                            'str_end':
                                {'name': u'Улица',
                                 'delegate': delegates.PlainTextDelegate,
                                 'editable': True},
                            'str_soc':
                                {'name': u'Тип улицы',
                                 'delegate': delegates.PlainTextDelegate,
                                 'editable': True},
                            'dom':
                                {'name': u'Дом',
                                 'delegate': delegates.PlainTextDelegate,
                                 'editable': True},
                            'numb':
                                {'name': u'Дом числ.',
                                 'delegate': delegates.IntegerDelegate,
                                 'editable': True},
                            'kv':
                                {'name': u'Квартира',
                                 'delegate': delegates.PlainTextDelegate,
                                 'editable': True},
                            'skv_uch':
                                {'delegate': delegates.PlainTextDelegate,
                                 'editable': True},
                            'house':
                                {'name': u'Адрес',
                                 'delegate': delegates.Many2OneDelegate,
                                 'admin': model.Patient.Admin(application.the_application, model.House),
                                 'editable': True},
                            # 'house':
                            #     {'name': u'Адрес',
                            #      'delegate': delegates.IntegerDelegate,
                            #      'editable': True},
                            'use_pol':
                                {'name': '', 'delegate': delegates.BoolDelegate,
                                 'editable': True},
                            'use_cla_end':
                                {'delegate': delegates.BoolDelegate,
                                 'editable': True},
                            'use_cla_soc':
                                {'delegate': delegates.BoolDelegate,
                                 'editable': True},
                            'use_str_end':
                                {'delegate': delegates.BoolDelegate,
                                 'editable': True},
                            'use_str_soc':
                                {'delegate': delegates.BoolDelegate,
                                 'editable': True},
                            'use_dom':
                                {'delegate': delegates.BoolDelegate,
                                 'editable': True},
                            'use_numb':
                                {'delegate': delegates.BoolDelegate,
                                 'editable': True},
                            'use_kv':
                                {'delegate': delegates.BoolDelegate,
                                 'editable': True},
                            'use_skv_uch':
                                {'delegate': delegates.BoolDelegate,
                                 'editable': True},
                            'use_house':
                                {'delegate': delegates.BoolDelegate,
                                 'editable': True}
                            }
        form_display = HBoxForm([
            ['pol', 'cla_end', 'cla_soc', 'str_end', 'str_soc', 'dom', 'numb', 'kv', 'skv_uch', 'house'],
            [WidgetOnlyForm('use_pol'), WidgetOnlyForm('use_cla_end'), WidgetOnlyForm('use_cla_soc'),
             WidgetOnlyForm('use_str_end'), WidgetOnlyForm('use_str_soc'), WidgetOnlyForm('use_dom'),
             WidgetOnlyForm('use_numb'), WidgetOnlyForm('use_kv'), WidgetOnlyForm('use_skv_uch'),
             WidgetOnlyForm('use_house')]])
        form_actions = [AddressSel(), CleanSel()]

        def __unicode__(self):
            full = []
            return u' '.join(full)


class AssignShapeAction(Action):
    verbose_name = _('Assign Shape')

    def model_run(self, model_context):
        if not application.the_application.patient_shape:
            application.the_application.patient_shape = PatientShape()
        for patient in model_context.get_selection():
            application.the_application.patient_shape.house = getattr(patient, 'house', None)
            application.the_application.patient_shape.pol = patient.pol
            application.the_application.patient_shape.cla_end = patient.cla_end
            application.the_application.patient_shape.cla_soc = patient.cla_soc
            application.the_application.patient_shape.str_end = patient.str_end
            application.the_application.patient_shape.str_soc = patient.str_soc
            application.the_application.patient_shape.dom = patient.dom
            application.the_application.patient_shape.numb = patient.numb
            application.the_application.patient_shape.kv = patient.kv
            application.the_application.patient_shape.skv_uch = patient.skv_uch

            application.the_application.patient_shape.use_nothing()
            break

        yield FlushSession(model_context.session)
        # yield Refresh()


class SetupByShapeAction(Action):
    verbose_name = _('Setup By Shape')

    def model_run(self, model_context):
        if not application.the_application.patient_shape:
            application.the_application.patient_shape = PatientShape()
        yield ChangeObject(application.the_application.patient_shape)
        for patient in model_context.get_selection():
            yield UpdateProgress(text=unicode(_('Setup %s')) % unicode(patient))
            if application.the_application.patient_shape.use_house:
                patient.house = application.the_application.patient_shape.house
            if application.the_application.patient_shape.use_pol:
                patient.pol = application.the_application.patient_shape.pol
            if application.the_application.patient_shape.use_cla_end:
                patient.cla_end = application.the_application.patient_shape.cla_end
            if application.the_application.patient_shape.use_cla_soc:
                patient.cla_soc = application.the_application.patient_shape.cla_soc
            if application.the_application.patient_shape.use_str_end:
                patient.str_end = application.the_application.patient_shape.str_end
            if application.the_application.patient_shape.use_str_soc:
                patient.str_soc = application.the_application.patient_shape.str_soc
            if application.the_application.patient_shape.use_dom:
                patient.dom = application.the_application.patient_shape.dom
            if application.the_application.patient_shape.use_numb:
                patient.numb = application.the_application.patient_shape.numb
            if application.the_application.patient_shape.use_kv:
                patient.kv = application.the_application.patient_shape.kv
            if application.the_application.patient_shape.use_skv_uch:
                patient.skv_uch = application.the_application.patient_shape.skv_uch
        yield FlushSession(model_context.session)
        # yield Refresh()
